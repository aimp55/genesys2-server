# TODO Historic records #

- Copy /explore to /archive to allow access to historic entries?
- Use archives to improve resolver service

# TODO KPI #

- Figure out how to trigger execution runs in a distributed environment
- Need a GUI to display observations
- Can we ignore observations that have the same result as previous execution run?
- How frequently do we trigger the runs?

# TODO PDCI #

- Use DS to hold results of calculation, similar to worldclim DS
- Re-calculation needs to happen after any update to Accession or AccessionRelated (same as ES update)
- PDCI dataset is downloadable as Excel (for selected accessions)
- WorldClim dataset is downloadable as Excel (for selected accessions)

