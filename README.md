
# Genesys PGR - Gateway to genetic resources


## Running genesys2-server with jetty

Download a **stable-9** copy of [jetty](http://download.eclipse.org/jetty/) and unpack it 
to your favorite location. Obtain a copy of `genesys2-server-X.Y.Z-jetty.zip` archive by 
building the project. The zip can be found in target/ directory.

Unpack `genesys2-server-X.Y.Z-jetty.zip` archive and move its contents into the jetty directory,
next to existing `demo-base` and `webapps` directories.

Start jetty from the `genesys2-server-X.Y.Z-jetty` base

	$ cd genesys2-server-X.Y.Z-jetty/
	$ java -jar ../start.jar

STDOUT is used for logging.

This configuration uses HSQL database and is intended for testing genesys2-server.
To change the database settings, edit `genesys2-server-X.Y.Z-jetty/resources/genesys.properties` file:

	# mysql
	db.url=jdbc:mysql://localhost/genesys?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false
	db.driverClassName=com.mysql.jdbc.Driver
	db.username=root
	db.password=mysql
	hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect

The complete list of available configuration options and their default values can be found
in [spring.properties](https://bitbucket.org/genesys2/genesys2-server/src/master/src/main/resources/spring/spring.properties).


## Running genesys2-server with maven

* Clone genesys2-server to your computer
* Create a blank mysql database
* Configure genesys2-server database connection settings
* Start Jetty with `mvn jetty:run`

### Creating a mysql database

Connect to your mysql instance and create a new blank database **genesys2**:

	# Create database
	CREATE DATABASE genesys2 DEFAULT CHARSET UTF8;

	# Create user genesys with password pwd
	GRANT ALL ON genesys2.* TO 'genesys'@'localhost' IDENTIFIED BY 'pwd';

Change the settings in `src/main/resources/spring/spring.properties`:

	# genesys2 with mysql database
	db.url=jdbc:mysql://localhost/genesys2?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false
	db.driverClassName=com.mysql.jdbc.Driver
	hibernate.dialect=org.hibernate.dialect.MySQL5Dialect
	db.username=genesys
	db.password=pwd
	db.showSql=false
	# Allow hibernate to update database (development/testing mode only)
	db.hbm2ddl=update

