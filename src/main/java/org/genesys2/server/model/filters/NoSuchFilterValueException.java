package org.genesys2.server.model.filters;

public class NoSuchFilterValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4696024324274047863L;
	private Object filterValue;

	public NoSuchFilterValueException(Object filterValue) {
		this.filterValue = filterValue;
	}

	public Object getFilterValue() {
		return filterValue;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Invalid value: " + filterValue;
	}
}
