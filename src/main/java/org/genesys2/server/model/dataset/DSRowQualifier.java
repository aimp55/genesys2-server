package org.genesys2.server.model.dataset;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ds2rowqualifier", indexes = { @Index(columnList = "dsq, vall") })
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.INTEGER, name = "typ")
public abstract class DSRowQualifier<T> {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY, optional = false)
	@JoinColumn(name = "r", updatable=false)
	private DSRow row;

	@ManyToOne(fetch=FetchType.LAZY, optional = false)
	@JoinColumn(name = "dsq", updatable=false)
	private DSQualifier datasetQualifier;

	public abstract T getValue();

	public abstract void setValue(T value);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DSRow getRow() {
		return row;
	}

	public void setRow(DSRow row) {
		this.row = row;
	}

	public DSQualifier getDatasetQualifier() {
		return datasetQualifier;
	}

	public void setDatasetQualifier(DSQualifier datasetQualifier) {
		this.datasetQualifier = datasetQualifier;
	}

	public static DSRowQualifier<?> make(Object qualifier) {
		if (qualifier == null) {
			return null;
		} else if (qualifier instanceof Long) {
			DSRowQualifierLong dsrq = new DSRowQualifierLong();
			dsrq.setValue((Long) qualifier);
			return dsrq;
		}
		return null;
	}

}
