/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.util.ArrayList;
import java.util.List;

public class ColumnGroups {
	List<Integer> columns = null;
	List<ColumnGroups> subs = null;

	public List<Integer> getColumns() {
		return columns;
	}

	public void setColumns(final List<Integer> columns) {
		this.columns = columns;
	}

	public List<ColumnGroups> getSubs() {
		return subs;
	}

	public void setSubs(final List<ColumnGroups> subs) {
		this.subs = subs;
	}

	public synchronized void addColumn(final int column) {
		if (columns == null) {
			columns = new ArrayList<Integer>();
		}
		columns.add(column);
	}

	public synchronized void addSub(final ColumnGroups subGroup) {
		if (subs == null) {
			subs = new ArrayList<ColumnGroups>();
		}
		subs.add(subGroup);
	}
}
