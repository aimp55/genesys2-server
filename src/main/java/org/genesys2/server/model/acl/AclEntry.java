/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.acl;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.BusinessModel;

@Entity
@Table(name = "acl_entry", uniqueConstraints = @UniqueConstraint(columnNames = { "acl_object_identity", "ace_order" }))
public class AclEntry extends BusinessModel {

	
	private static final long serialVersionUID = -1047000445685485825L;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "acl_object_identity", nullable = false)
	private AclObjectIdentity aclObjectIdentity;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "sid", nullable = false)
	private AclSid aclSid;

	@Column(name = "ace_order", nullable = false, length = 11)
	private long aceOrder;

	@Column(name = "mask", nullable = false, length = 11)
	private long mask;

	@Column(name = "granting", nullable = false, length = 1)
	private boolean granting;

	@Column(name = "audit_success", nullable = false, length = 1)
	private boolean auditSuccess;

	@Column(name = "audit_failure", nullable = false, length = 1)
	private boolean auditFailure;

	public AclObjectIdentity getAclObjectIdentity() {
		return aclObjectIdentity;
	}

	public void setAclObjectIdentity(AclObjectIdentity aclObjectIdentity) {
		this.aclObjectIdentity = aclObjectIdentity;
	}

	public AclSid getAclSid() {
		return aclSid;
	}

	public void setAclSid(AclSid aclSid) {
		this.aclSid = aclSid;
	}

	public long getAceOrder() {
		return aceOrder;
	}

	public void setAceOrder(long aceOrder) {
		this.aceOrder = aceOrder;
	}

	public long getMask() {
		return mask;
	}

	public void setMask(long mask) {
		this.mask = mask;
	}

	public boolean isGranting() {
		return granting;
	}

	public void setGranting(boolean granting) {
		this.granting = granting;
	}

	public boolean isAuditSuccess() {
		return auditSuccess;
	}

	public void setAuditSuccess(boolean auditSuccess) {
		this.auditSuccess = auditSuccess;
	}

	public boolean isAuditFailure() {
		return auditFailure;
	}

	public void setAuditFailure(boolean auditFailure) {
		this.auditFailure = auditFailure;
	}
}
