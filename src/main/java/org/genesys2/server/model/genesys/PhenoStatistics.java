/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.io.Serializable;

/**
 * Statistics for phenotypic data
 */
public class PhenoStatistics implements Serializable {
	private static final long serialVersionUID = 2239103854339545879L;
	
	private long count;
	private long withMethod;
	private long witMetadata;
	private float avgMethods;
	private float avgMetadatas;

	public void setCount(long count) {
		this.count = count;
	}

	public long getCount() {
		return count;
	}

	public void setWithMethod(long withMethod) {
		this.withMethod = withMethod;
	}

	public long getWithMethod() {
		return withMethod;
	}

	public float getWithMethodPercent() {
		return (float) withMethod / count;
	}

	public void setWithMetadata(long withMetadata) {
		this.witMetadata = withMetadata;
	}

	public long getWitMetadata() {
		return witMetadata;
	}

	public float getWithMetadataPercent() {
		return (float) witMetadata / count;
	}

	public void setAvgMethods(float avg) {
		this.avgMethods = avg;
	}

	public float getAvgMethods() {
		return avgMethods;
	}

	public void setAvgMetadatas(float avg) {
		this.avgMetadatas = avg;
	}

	public float getAvgMetadatas() {
		return avgMetadatas;
	}

	public Object[] getElStats() {
		return new Object[] { count, withMethod, getWithMethodPercent(), avgMethods, avgMetadatas };
	}

}
