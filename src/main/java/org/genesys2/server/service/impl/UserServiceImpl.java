/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.NoUserFoundException;
import org.genesys2.server.exception.NotUniqueUserException;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.wrapper.UserWrapper;
import org.genesys2.server.persistence.domain.UserPersistence;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.UserService;
import org.genesys2.spring.SecurityContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
	private static final Log LOG = LogFactory.getLog(UserServiceImpl.class);

	@Autowired
	private UserPersistence userPersistence;

	@Autowired(required = false)
	private final PasswordEncoder passwordEncoder = NoOpPasswordEncoder.getInstance();

	private long accountLockoutTime = 5 * 60 * 1000;

	private final List<UserRole> availableRoles = ListUtils.unmodifiableList(Arrays.asList(UserRole.values()));

	/**
	 * Set number of milliseconds for user account lockout
	 */
	public void setAccountLockoutTime(long accountLockoutTime) {
		this.accountLockoutTime = accountLockoutTime;
	}

	public long getAccountLockoutTime() {
		return accountLockoutTime;
	}

	@Override
	public List<UserRole> listAvailableRoles() {
		return availableRoles;
	}

	@Transactional(readOnly = false)
	@Override
	public UserDetails getUserDetails(String userUuid) {
		return getUserDetails(getUserByUuid(userUuid));
	}

	@Transactional(readOnly = false)
	@Override
	public UserDetails getUserDetails(User user) {
		if (user == null) {
			return null;
		}
		final boolean enabled = user.isEnabled();
		final boolean accountNonExpired = !user.isAccountExpired();
		final boolean credentialsNonExpired = !user.isPasswordExpired();
		final boolean accountNonLocked = !user.isAccountLocked();

		if (!accountNonLocked) {
			LOG.warn("Account is locked for user=" + user.getEmail() + " until=" + user.getLockedUntil());
		}
		if (!enabled) {
			LOG.warn("Account is disabled for user=" + user.getEmail());
		}

		if (user.getLockedUntil() != null && accountNonLocked) {
			LOG.warn("Account can be unlocked for user=" + user.getEmail());
			user.setLockedUntil(null);
			userPersistence.save(user);
		}

		final AuthUserDetails userDetails = new AuthUserDetails(user.getUuid(), user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, getGrantedAuthorities(user));

		// set actual DB user for possible further purposes
		userDetails.setUser(user);

		return userDetails;
	}

	private Collection<? extends GrantedAuthority> getGrantedAuthorities(User user) {
		final List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

		for (final UserRole userRole : user.getRoles()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(userRole.name()));
		}

		return grantedAuthorities;
	}

	@Override
	// FIXME Re-enable this
	// @PreAuthorize("hasRole('ADMINISTRATOR')")
	public Page<User> listUsers(Pageable pageable) {
		return userPersistence.findAll(pageable);
	}

	@Override
	@Transactional
	public UserWrapper getWrappedById(long userId) throws UserException {
		try {
			final User user = userPersistence.findOne(userId);

			if (user == null) {
				throw new NoUserFoundException(userId);
			}

			return transformUserToWrapper(user);
		} catch (final RuntimeException e) {
			throw new UserException(e);
		}
	}

	@Override
	public Page<UserWrapper> listWrapped(int startRow, int pageSize) throws UserException {
		Pageable pageable = null;
		if (startRow >= 0 && pageSize > 0) {
			pageable = new PageRequest(startRow / pageSize, pageSize);
		}
		try {
			final Page<User> users = userPersistence.findAll(pageable);
			final List<UserWrapper> userWrappers = new ArrayList<UserWrapper>();
			for (final User user : users) {
				userWrappers.add(transformUserToWrapper(user));
			}
			return new PageImpl<UserWrapper>(userWrappers, pageable, users.getTotalElements());
		} catch (final RuntimeException e) {
			throw new UserException(e);
		}
	}

	private UserWrapper transformUserToWrapper(User user) throws UserException {
		final UserWrapper userWrapper = new UserWrapper();
		userWrapper.setId(user.getId());
		userWrapper.setName(user.getName());
		userWrapper.setEmail(user.getEmail());
		userWrapper.setPassword(user.getPassword());
		final Set<UserRole> roles = user.getRoles();
		roles.size();
		userWrapper.setRoles(roles);
		return userWrapper;
	}

	@Override
	@Transactional(readOnly = false)
	public User createAccount(String email, String initialPassword, String fullName) {
		final User user = new User();
		user.setEmail(email);
		user.setName(fullName);
		user.getRoles().add(UserRole.USER);
		setPassword(user, initialPassword);

		userPersistence.save(user);
		return user;
	}

	/**
	 * @param user
	 * @throws UserException
	 */
	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false, rollbackFor = NotUniqueUserException.class)
	public void addUser(User user) throws UserException {
		try {
			if (user.isSystemAccount()) {
				user.setPassword("THIS-IS-NOT-A-PASSWORD");
			} else {
				final String rawPassword = user.getPassword();
				// encrypt password
				user.setPassword(passwordEncoder.encode(rawPassword));
			}

			// save user
			userPersistence.save(user);
		} catch (final DataIntegrityViolationException e) {
			throw new NotUniqueUserException(e, user.getEmail());
		} catch (final EmptyResultDataAccessException e) {
			throw new NoUserFoundException(e, user.getId());
		} catch (final RuntimeException e) {
			throw new UserException(e);
		}
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = NotUniqueUserException.class)
	public void updateUser(User user) throws UserException {
		try {
			userPersistence.save(user);
		} catch (final DataIntegrityViolationException e) {
			throw new NotUniqueUserException(e, user.getEmail());
		} catch (final EmptyResultDataAccessException e) {
			throw new NoUserFoundException(e, user.getId());
		} catch (final RuntimeException e) {
			throw new UserException(e);
		}
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.id == #userId")
	@Transactional(readOnly = false)
	public User updateData(long userId, String name, String email) {
		final User user = userPersistence.findOne(userId);
		user.setName(name);
		user.setEmail(email);
		userPersistence.save(user);
		return user;
	}

	@Override
	@Transactional(readOnly = false)
	public void updatePassword(long userId, String rawPassword) throws UserException {
		final User user = userPersistence.findOne(userId);
		setPassword(user, rawPassword);
		userPersistence.save(user);
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public void setAccountEnabled(String uuid, boolean enabled) {
		final User user = userPersistence.findByUuid(uuid);
		if (!enabled && user.getRoles().contains(UserRole.ADMINISTRATOR)) {
			throw new SecurityException("Can't disable ADMINISTRATOR accounts");
		}
		user.setEnabled(enabled);
		userPersistence.save(user);
		LOG.warn("User account for user=" + user.getEmail() + " enabled=" + enabled);
	}

	/**
	 * For internal use only.
	 */
	@Override
	@Transactional(readOnly = false)
	public void setAccountLockLocal(String uuid, boolean locked) {
		final User user = userPersistence.findByUuid(uuid);
		if (locked) {
			// Lock for account until some time
			user.setLockedUntil(new Date(System.currentTimeMillis() + accountLockoutTime));
			LOG.warn("Locking user account for user=" + user.getEmail() + "  until=" + user.getLockedUntil());
		} else {
			LOG.warn("Unlocking user account for user=" + user.getEmail());
			user.setLockedUntil(null);
		}
		userPersistence.save(user);
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public void setAccountLock(String uuid, boolean locked) {
		setAccountLockLocal(uuid, locked);
	}

	private void setPassword(User user, String rawPassword) {
		// encrypt password
		user.setPassword(passwordEncoder.encode(rawPassword));
	}

	@Override
	@Transactional(readOnly = false)
	public void removeUser(User user) throws UserException {
		try {
			userPersistence.delete(user);
		} catch (final EmptyResultDataAccessException e) {
			throw new NoUserFoundException(e, user.getId());
		} catch (final RuntimeException e) {
			throw new UserException(e);
		}
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = NoUserFoundException.class)
	public void removeUserById(long userId) throws UserException {
		try {
			userPersistence.delete(userId);
		} catch (final EmptyResultDataAccessException e) {
			throw new NoUserFoundException(e, userId);
		} catch (final RuntimeException e) {
			throw new UserException(e);
		}
	}

	@Override
	@PreAuthorize("isAuthenticated()")
	public User getMe() {
		final User user = userPersistence.findByUuid(SecurityContextUtil.getCurrentUser().getUuid());

		if (user != null) {
			user.getRoles().size();
		}

		return user;
	}

	@Override
	public User getUserByEmail(String email) {
		final User user = userPersistence.findByEmail(email);

		if (user == null) {
			throw new UsernameNotFoundException(email);
		}

		user.getRoles().size();
		return user;
	}

	@Override
	public User getUserByUuid(String uuid) {
		final User user = userPersistence.findByUuid(uuid);

		if (user == null) {
			throw new UsernameNotFoundException(uuid);
		}

		user.getRoles().size();
		return user;
	}

	@Override
	public User getSystemUser(String username) {
		return userPersistence.findSystemUser(username);
	}

	@Override
	public User getUserById(long userId) throws UserException {
		try {
			final User user = userPersistence.findOne(userId);

			if (user == null) {
				throw new NoUserFoundException(userId);
			}
			user.getRoles().size();
			return user;
		} catch (final RuntimeException e) {
			throw new UserException(e);
		}
	}

	@Override
	public boolean exists(String username) {
		return userPersistence.findByEmail(username) != null;
	}

	@Override
	@Transactional
	public void userEmailValidated(String uuid) {
		LOG.info("Validating user email for uuid=" + uuid);

		final User user = userPersistence.findByUuid(uuid);
		Set<UserRole> userRoles = user.getRoles();
		if (userRoles == null) {
			LOG.debug("User roles are null, creating role set");
			user.setRoles(userRoles = new HashSet<UserRole>());
		}
		// Since it's a set, we can just add
		userRoles.add(UserRole.VALIDATEDUSER);

		try {
			updateUser(user);
			addRoleToCurrentUser(user, UserRole.VALIDATEDUSER.getName());
			LOG.info("Ensured VALIDATEDUSER role for user " + user);
		} catch (final UserException e) {

		}
	}

	@Override
	@Transactional
	public void addVettedUserRole(String uuid) {
		final User user = userPersistence.findByUuid(uuid);
		final Set<UserRole> userRoles = user.getRoles();
		userRoles.add(UserRole.VETTEDUSER);

		try {
			updateUser(user);
			addRoleToCurrentUser(user, UserRole.VETTEDUSER.getName());
			LOG.info("Add role VETTEDUSER for user " + user);
		} catch (final UserException e) {
			LOG.error(e);
		}

	}

	private void addRoleToCurrentUser(User user, String role) {
		final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof AuthUserDetails) {
			if (! ((AuthUserDetails) principal).getUser().getId().equals(user.getId())) {
				LOG.warn("Not adding role, user != principal");
				return;
			}
		} else {
			LOG.warn("Not adding role to current principal: " + principal);
			return;
		}

		final List<GrantedAuthority> authorities = new ArrayList<>(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
		for (final GrantedAuthority authority : authorities) {
			if (authority.getAuthority().equals(role)) {
				return;
			}
		}
		final SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(role);
		authorities.add(simpleGrantedAuthority);

		final Authentication authentication = new UsernamePasswordAuthenticationToken(principal, principal, authorities);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	/**
	 * Replace user's roles with roles specified in the list
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Override
	@Transactional(readOnly = false)
	public void updateRoles(User user, List<String> selectedRoles) {
		final User dbuser = userPersistence.findOne(user.getId());
		LOG.info("Removing all roles for user " + user);
		dbuser.getRoles().clear();
		for (final String role : selectedRoles) {
			dbuser.getRoles().add(UserRole.valueOf(role));
			LOG.info("Adding role " + role + " to user " + user);
		}
		userPersistence.save(dbuser);
	}

	@PreAuthorize("isAuthenticated()")
	@Override
	public List<User> autocompleteUser(String email) {
		if (StringUtils.isBlank(email) || email.length() < 4)
			return Collections.emptyList();
		return userPersistence.autocompleteByEmail(email + "%", new PageRequest(0, 10, new Sort("email")));
	}
}
