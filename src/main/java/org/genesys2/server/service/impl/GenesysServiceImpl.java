/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.persistence.EntityManager;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionAlias;
import org.genesys2.server.model.genesys.AccessionAlias.AliasType;
import org.genesys2.server.model.genesys.AccessionBreeding;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.genesys.AccessionExchange;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.genesys.AccessionHistoric;
import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.genesys.AccessionRemark;
import org.genesys2.server.model.genesys.AccessionTrait;
import org.genesys2.server.model.genesys.AllAccnames;
import org.genesys2.server.model.genesys.ExperimentAccessionTrait;
import org.genesys2.server.model.genesys.ExperimentTrait;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.PDCI;
import org.genesys2.server.model.genesys.PDCIStatistics;
import org.genesys2.server.model.genesys.PhenoStatistics;
import org.genesys2.server.model.genesys.SelfCopy;
import org.genesys2.server.model.genesys.SvalbardData;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.genesys.TraitCode;
import org.genesys2.server.model.impl.AccessionIdentifier3;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.server.persistence.domain.AccessionAliasRepository;
import org.genesys2.server.persistence.domain.AccessionBreedingRepository;
import org.genesys2.server.persistence.domain.AccessionCollectRepository;
import org.genesys2.server.persistence.domain.AccessionExchangeRepository;
import org.genesys2.server.persistence.domain.AccessionGeoRepository;
import org.genesys2.server.persistence.domain.AccessionHistoricRepository;
import org.genesys2.server.persistence.domain.AccessionIdRepository;
import org.genesys2.server.persistence.domain.AccessionNameRepository;
import org.genesys2.server.persistence.domain.AccessionRemarkRepository;
import org.genesys2.server.persistence.domain.AccessionRepository;
import org.genesys2.server.persistence.domain.AccessionTraitRepository;
import org.genesys2.server.persistence.domain.CropTaxonomyRepository;
import org.genesys2.server.persistence.domain.FaoInstituteRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.persistence.domain.MetadataMethodRepository;
import org.genesys2.server.persistence.domain.MetadataRepository;
import org.genesys2.server.persistence.domain.MethodRepository;
import org.genesys2.server.persistence.domain.PDCIRepository;
import org.genesys2.server.persistence.domain.SvalbardRepository;
import org.genesys2.server.persistence.domain.TraitValueRepository;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.DatasetService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.spring.SecurityContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.bytecode.opencsv.CSVWriter;
import au.com.bytecode.opencsv.ResultSetHelper;
import au.com.bytecode.opencsv.ResultSetHelperService;

@Service
@Transactional(readOnly = true)
public class GenesysServiceImpl implements GenesysService, DatasetService {

	public static final Log LOG = LogFactory.getLog(GenesysServiceImpl.class);

	@Autowired
	private EntityManager entityManager;

	// Services
	@Autowired
	private TaxonomyService taxonomyService;
	@Autowired
	private AclService aclService;
	@Autowired
	private HtmlSanitizer htmlSanitizer;

	// Repositories
	@Autowired
	private AccessionRepository accessionRepository;
	@Autowired
	private AccessionHistoricRepository accessionHistoricRepository;
	@Autowired
	private AccessionIdRepository accessionIdRepository;

	@Autowired
	private AccessionBreedingRepository accessionBreedingRepository;
	@Autowired
	private AccessionCollectRepository accessionCollectRepository;
	@Autowired
	private AccessionNameRepository accessionNamesRepository;
	@Autowired
	private AccessionExchangeRepository accessionExchangeRepository;
	@Autowired
	private AccessionGeoRepository accessionGeoRepository;
	@Autowired
	private AccessionTraitRepository accessionTraitRepository;
	@Autowired
	private MetadataRepository metadataRepository;
	@Autowired
	private MetadataMethodRepository metadataMethodRepository;
	@Autowired
	private TraitValueRepository traitValueRepository;
	@Autowired
	private MethodRepository methodRepository;
	@Autowired
	private CropTaxonomyRepository cropTaxonomyRepository;
	@Autowired
	private OrganizationService organizationService;
	@Autowired
	private FaoInstituteRepository instituteRepository;
	@Autowired
	private GenesysLowlevelRepository genesysLowlevelRepository;
	@Autowired
	private SvalbardRepository svalbardRepository;
	@Autowired
	private AccessionAliasRepository accessionAliasRepository;
	@Autowired
	private AccessionRemarkRepository accessionRemarkRepository;

	@Autowired
	private CropService cropService;

	@Autowired(required = false)
	private PDCICalculator pdciCalculator;

	@Autowired
	private PDCIRepository repoPdci;

	@Override
	public long countAll() {
		return accessionRepository.count();
	}

	@Override
	public long countByInstitute(FaoInstitute institute) {
		return accessionRepository.countByInstitute(institute);
	}

	@Override
	public long countByOrigin(Country country) {
		return accessionRepository.countByOrigin(country);
	}

	@Override
	public long countByLocation(Country country) {
		return accessionRepository.countByLocation(country);
	}

	@Override
	public List<FaoInstitute> findHoldingInstitutes(Set<Long> accessionIds) {
		return accessionRepository.findDistinctInstitutesFor(accessionIds);
	}

	@Override
	public Set<Long> listAccessions(FaoInstitute institute, Set<Long> accessionIds) {
		return accessionRepository.findByInstituteAndIds(institute, accessionIds);
	}

	@Override
	public Page<Accession> listAccessions(Collection<Long> accessionIds, Pageable pageable) {
		if (accessionIds == null || accessionIds.size() == 0) {
			return null;
		}
		return accessionRepository.findById(accessionIds, pageable);
	}

	@Override
	public List<Accession> listAccessionsSGSV(List<? extends AccessionIdentifier3> accns) {
		final List<Accession> result = new ArrayList<Accession>(accns.size());
		for (final AccessionIdentifier3 aid3 : accns) {

			Accession accn = null;

			try {
				accn = accessionRepository.findOneSGSV(aid3.getHoldingInstitute(), aid3.getAccessionName(), aid3.getGenus());
			} catch (IncorrectResultSizeDataAccessException e) {
				LOG.warn("Duplicate entry for " + aid3);
			}

			if (accn != null) {
				result.add(accn);
			} else {

				try {
					Accession accnByAlias = accessionAliasRepository.findAccession(aid3.getHoldingInstitute(), aid3.getAccessionName(),
							AccessionAlias.AliasType.OTHERNUMB.getId());

					if (accnByAlias != null) {
						LOG.info("Found accession by alias " + accnByAlias);
						// Genus must match
						if (StringUtils.equalsIgnoreCase(aid3.getGenus(), accnByAlias.getTaxonomy().getGenus()))
							result.add(accnByAlias);
						else {
							LOG.info("... but genus doesn't match");
							result.add(null);
						}
					} else {
						result.add(null);

						if (LOG.isDebugEnabled()) {
							// Only log full miss
							LOG.debug("No accession " + aid3);
						}
					}
				} catch (IncorrectResultSizeDataAccessException e) {
					LOG.warn("Multple accessions with alias " + aid3);
					result.add(null);
				}
			}
		}
		return result;
	}

	@Override
	public Accession getAccession(AccessionIdentifier3 aid3) throws NonUniqueAccessionException {
		try {
			Accession accession = accessionRepository.findOne(aid3.getHoldingInstitute(), aid3.getAccessionName(), aid3.getGenus());
			if (accession != null)
				accession.getStoRage().size();
			return accession;
		} catch (IncorrectResultSizeDataAccessException e) {
			LOG.error("Duplicate accession name instCode=" + aid3.getHoldingInstitute() + " acceNumb=" + aid3.getAccessionName() + " genus=" + aid3.getGenus());
			throw new NonUniqueAccessionException(aid3.getHoldingInstitute(), aid3.getAccessionName(), aid3.getGenus());
		}
	}

	@Override
	public Accession getAccession(String instCode, String acceNumb) throws NonUniqueAccessionException {
		try {
			Accession accession = accessionRepository.findByInstituteCodeAndAccessionName(instCode, acceNumb);
			if (accession != null)
				accession.getStoRage().size();
			return accession;
		} catch (IncorrectResultSizeDataAccessException e) {
			LOG.error("Duplicate accession name instCode=" + instCode + " acceNumb=" + acceNumb);
			throw new NonUniqueAccessionException(instCode, acceNumb);
		}
	}

	@Override
	public Accession getAccession(String instCode, String acceNumb, String genus) throws NonUniqueAccessionException {
		if (genus == null) {
			throw new NullPointerException("Genus is required to load accession by instCode, acceNumb and genus");
		}
		try {
			Accession accession = accessionRepository.findOne(instCode, acceNumb, genus);
			if (accession != null)
				accession.getStoRage().size();
			return accession;
		} catch (IncorrectResultSizeDataAccessException e) {
			LOG.error("Duplicate accession name instCode=" + instCode + " acceNumb=" + acceNumb);
			throw new NonUniqueAccessionException(instCode, acceNumb);
		}
	}

	@Override
	public Accession getAccession(long accessionId) {
		Accession accession = accessionRepository.findOne(accessionId);
		if (accession != null) {
			accession.getStoRage().size();
			if (accession.getCountryOfOrigin() != null)
				accession.getCountryOfOrigin().getId();
		}
		return accession;
	}

	@Override
	public AccessionDetails getAccessionDetails(long accessionId) {
		AccessionData accession = getAccessionData(accessionId);
		if (accession == null) {
			LOG.warn("No such accession with id=" + accessionId);
			return null;
		}

		return toAccessionDetails(loadAllStuff(accession));
	}

	private AccessionDetails toAccessionDetails(AllStuff all) {
		// LOG.info("Converting to AccessionDetails " + all);
		if (all == null || all.accession == null)
			return null;

		AccessionDetails ad = AccessionDetails.from(all.accession);
		ad.networks(organizationService.getOrganizations(all.accession.getInstitute()));
		ad.aliases(all.names);
		ad.exch(all.exch);
		ad.collect(all.collect);
		ad.breeding(all.bred);
		ad.geo(all.geo);
		ad.svalbard(all.svalbard);
		ad.remarks(all.remarks);
		// ad.traits(listMethods(accession),
		// getAccessionTraitValues(accession));
		ad.crops(cropService.getCrops(all.accession.getTaxonomy()));
		return ad;
	}

	@Override
	public Set<AccessionDetails> getAccessionDetails(Collection<Long> accessionIds) {
		Set<AccessionDetails> set = new HashSet<AccessionDetails>(accessionIds.size());
		List<AllStuff> alls = loadAllStuff(accessionIds);
		for (AllStuff all : alls) {
			if (all == null)
				continue;

			set.add(toAccessionDetails(all));
		}
		return set;
	}

	@Override
	public List<Accession> listAccessions(FaoInstitute faoInstitute, String accessionName) {
		return accessionRepository.findByInstituteAndAccessionName(faoInstitute, accessionName);
	}

	@Override
	public AccessionBreeding listAccessionBreeding(AccessionId accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionBreedingRepository.findByAccession(accession);
	}

	@Override
	public AccessionGeo listAccessionGeo(AccessionId accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionGeoRepository.findByAccession(accession);
	}

	@Override
	public List<AccessionGeo> listAccessionsGeo(Set<Long> copy) {
		if (copy == null || copy.isEmpty()) {
			return null;
		}
		return accessionGeoRepository.findForAccessions(copy);
	}

	@Override
	public SvalbardData getSvalbardData(AccessionId accession) {
		if (accession == null) {
			return null;
		}
		return svalbardRepository.findOne(accession.getId());
	}

	@Override
	public AccessionCollect listAccessionCollect(AccessionId accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionCollectRepository.findByAccession(accession);
	}

	@Override
	public AccessionExchange listAccessionExchange(AccessionId accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionExchangeRepository.findByAccession(accession);
	}

	@Override
	public List<AccessionRemark> listAccessionRemarks(AccessionId accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionRemarkRepository.findByAccession(accession);
	}

	@Override
	public AllAccnames listAccessionNames(AccessionId accession) {
		return accessionNamesRepository.findByAccession(accession);
	}

	@Override
	public List<AccessionAlias> listAccessionAliases(AccessionId accession) {
		return accessionAliasRepository.findByAccession(accession);
	}

	public AllStuff loadAllStuff(AccessionData accession) {
		if (accession == null)
			return null;
		AllStuff all = new AllStuff(accession);
		all.bred = listAccessionBreeding(accession.getAccessionId());
		all.collect = listAccessionCollect(accession.getAccessionId());
		all.exch = listAccessionExchange(accession.getAccessionId());
		all.geo = listAccessionGeo(accession.getAccessionId());
		all.names = listAccessionAliases(accession.getAccessionId());
		all.remarks = listAccessionRemarks(accession.getAccessionId());
		all.svalbard = getSvalbardData(accession.getAccessionId());
		return all;
	}

	@Override
	public List<AllStuff> loadAllStuff(Collection<Long> accessionIds) {
		List<AllStuff> alls = new ArrayList<AllStuff>(accessionIds.size());
		if (accessionIds == null || accessionIds.size() == 0) {
			return alls;
		}
		HashMap<Long, AllStuff> map = new HashMap<Long, AllStuff>();

		for (Long accessionId : accessionIds) {
			AllStuff all = new AllStuff(accessionId);
			alls.add(all);
			map.put(accessionId, all);
		}
		for (AccessionData a : accessionRepository.findAll(accessionIds)) {
			AllStuff all = map.get(a.getAccessionId().getId());
			all.accession = a;
			// Don't use a#getStorage()
			a.getStoRage().size();
			all.names = new ArrayList<AccessionAlias>();
			all.remarks = new ArrayList<AccessionRemark>();
		}

		for (AccessionCollect c : accessionCollectRepository.findAllFor(accessionIds)) {
			AllStuff all = map.get(c.getAccession().getId());
			all.collect = c;
		}

		for (AccessionGeo g : accessionGeoRepository.findAllFor(accessionIds)) {
			AllStuff all = map.get(g.getAccession().getId());
			all.geo = g;
		}

		for (AccessionBreeding b : accessionBreedingRepository.findAllFor(accessionIds)) {
			AllStuff all = map.get(b.getAccession().getId());
			all.bred = b;
		}

		for (AccessionExchange e : accessionExchangeRepository.findAllFor(accessionIds)) {
			AllStuff all = map.get(e.getAccession().getId());
			all.exch = e;
		}

		for (SvalbardData e : svalbardRepository.findAllFor(accessionIds)) {
			AllStuff all = map.get(e.getId());
			all.svalbard = e;
		}

		for (AccessionAlias aa : accessionAliasRepository.findAllFor(accessionIds)) {
			if (aa == null) {
				System.err.println("aa is null");
				continue;
			}
			AllStuff all = map.get(aa.getAccession().getId());
			if (all == null) {
				System.err.println(aa.getAccession().getId() + " not in the list! " + aa);
			} else {
				all.names.add(aa);
			}
		}

		for (AccessionRemark ar : accessionRemarkRepository.findAllFor(accessionIds)) {
			AllStuff all = map.get(ar.getAccession().getId());
			all.remarks.add(ar);
		}

		for (AllStuff all : alls) {
			detach(all.accession);
			detach(all.geo);
			detach(all.collect);
			detach(all.bred);
			detach(all.names);
			detach(all.exch);
			detach(all.remarks);
		}

		return alls;
	}

	private void detach(Object obj) {
		if (obj != null) {
			if (obj instanceof Collection) {
				for (Object o : (Collection<?>) obj)
					detach(o);
			} else {
				entityManager.detach(obj);
			}
		}
	}

	@Override
	public List<Metadata> listMetadata(AccessionId accession) {
		final List<Long> x = accessionTraitRepository.listMetadataIds(accession);
		return x.size() == 0 ? null : metadataRepository.findByIds(x);
	}

	@Override
	public Page<Metadata> listMetadata(Pageable pageable) {
		return metadataRepository.findAll(pageable);
	}

	@Override
	public Page<Metadata> listDatasets(FaoInstitute faoInstitute, Pageable pageable) {
		return metadataRepository.findByInstituteCode(faoInstitute.getCode(), pageable);
	}

	@Override
	public long countDatasets(FaoInstitute faoInstitute) {
		return metadataRepository.countByInstituteCode(faoInstitute.getCode());
	}

	@Override
	public Metadata getMetadata(long metadataId) {
		return metadataRepository.findOne(metadataId);
	}

	@Override
	public List<Method> listMethods(Metadata metadata) {
		final List<Long> x = metadataMethodRepository.listMetadataMethods(metadata);
		return x.size() == 0 ? null : methodRepository.findByIds(x);
	}

	@Override
	public Page<AccessionData> listMetadataAccessions(long metadataId, Pageable pageable) {
		Page<AccessionId> x = accessionTraitRepository.listMetadataAccessions(metadataId, pageable);
		List<AccessionData> content = listAccessionData(x.getContent());
		return new PageImpl<AccessionData>(content, pageable, x.getTotalElements());
	}

	private List<AccessionData> listAccessionData(List<AccessionId> accessionIds) {
		List<AccessionData> result = new ArrayList<AccessionData>(accessionIds.size());
		for (AccessionId accessionId : accessionIds) {
			result.add(getAccessionData(accessionId));
		}
		return result;
	}

	private AccessionData getAccessionData(AccessionId accessionId) {
		if (accessionId == null)
			return null;
		return getAccessionData(accessionId.getId());
	}

	private AccessionData getAccessionData(Long id) {
		Accession a = accessionRepository.findOne(id);
		if (a != null) {
			return a;
		} else {
			AccessionHistoric ah = accessionHistoricRepository.findOne(id);
			return ah;
		}
	}

	@Override
	public Map<Long, List<ExperimentTrait>> getAccessionTraitValues(AccessionId accession) {
		LOG.debug("Getting trait values for accession: " + accession);
		final List<AccessionTrait> x = accessionTraitRepository.findByAccession(accession);
		return x.size() == 0 ? null : traitValueRepository.getValues(x);
	}

	@Override
	public Map<Long, Map<Long, List<Object>>> getMetadataTraitValues(Metadata metadata, List<AccessionData> accessions) {
		final List<Method> methods = methodRepository.findByIds(metadataMethodRepository.listMetadataMethods(metadata));
		return traitValueRepository.getValues(metadata, methods, accessions);
	}

	@Override
	public Page<Object[]> statisticsGenusByInstitute(FaoInstitute institute, Pageable pageable) {
		return accessionRepository.statisticsGenusInInstitute(institute, pageable);
	}

	@Override
	public Page<Object[]> statisticsSpeciesByInstitute(FaoInstitute institute, Pageable pageable) {
		final Page<Object[]> page = accessionRepository.statisticsSpeciesInInstitute(institute, pageable);
		for (final Object[] r : page.getContent()) {
			if (r[0] != null) {
				r[0] = taxonomyService.get((Long) r[0]);
			}
		}
		return page;
	}

	@Override
	public List<Long> listAccessionsIds(Pageable pageable) {
		return accessionRepository.listAccessionsIds(pageable);
	}

	@Override
	public Page<Accession> listAccessionsByCrop(Crop crop, Pageable pageable) {
		final List<Taxonomy2> taxonomies = cropTaxonomyRepository.findTaxonomiesByCrop(crop);
		if (taxonomies == null || taxonomies.size() == 0) {
			return null;
		}
		return accessionRepository.findByTaxonomy(taxonomies, pageable);
	}

	@Override
	public Page<Accession> listAccessionsByOrganization(Organization organization, Pageable pageable) {
		final List<FaoInstitute> members = organizationService.getMembers(organization);
		if (members == null || members.size() == 0) {
			return new PageImpl<Accession>(Accession.EMPTY_LIST);
		}
		return accessionRepository.findByInstitute(members, pageable);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public void updateAccessionCountryRefs() {
		genesysLowlevelRepository.updateCountryRefs();
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public void updateAccessionInstitueRefs() {
		genesysLowlevelRepository.updateFaoInstituteRefs();
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public void updateAccessionCount(FaoInstitute institute) {
		if (institute == null)
			return;
		long accessionCount = accessionRepository.countByInstitute(institute);
		institute.setAccessionCount(accessionCount);
		instituteRepository.save(institute);
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public void setInSvalbard(List<Accession> matching) {
		if (matching.size() > 0) {
			accessionRepository.setInSvalbard(matching);
		}
	}

	/**
	 * @deprecated Should be removed
	 */
	@Deprecated
	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public void addAccessions(List<Accession> accessions) {
		accessionRepository.save(accessions);
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<Accession> saveAccessions(FaoInstitute institute, List<Accession> accessions) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Saving " + accessions.size() + " accessions");
		}
		accessionRepository.save(accessions);
		// updateAccessionCount(institute);
		return accessions;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public AccessionData saveAccession(AccessionData accession) {
		if (LOG.isDebugEnabled())
			LOG.debug("Updating " + accession);

		System.err.println("Saving " + accession + " ST=" + accession.getStorage());
		AccessionData res = null;
		if (accession instanceof AccessionHistoric) {
			res = accessionHistoricRepository.save((AccessionHistoric) accession);
		} else {
			res = accessionRepository.save((Accession) accession);
		}
		return res;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<Accession> saveAccessions(Iterable<Accession> accessions) {
		Set<FaoInstitute> institutes = new HashSet<FaoInstitute>();
		for (Accession accession : accessions) {
			System.out.println("Saving " + accession + " STO=" + accession.getStoRage() + " ST=" + accession.getStorage());
			institutes.add(accession.getInstitute());
		}
		List<Accession> res = accessionRepository.save(accessions);
		for (FaoInstitute institute : institutes)
			updateAccessionCount(institute);
		return res;
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'DELETE') or hasPermission(#institute, 'MANAGE')")
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionHistoric> removeAccessions(FaoInstitute institute, List<Accession> toDelete) {
		List<AccessionHistoric> deleted = new ArrayList<AccessionHistoric>();

		if (toDelete.size() > 0) {
			final Set<Long> accessionIds = new HashSet<Long>();

			for (final Accession accn : toDelete) {
				if (institute.getId().equals(accn.getInstitute().getId()))
					accessionIds.add(accn.getAccessionId().getId());
				else
					throw new RuntimeException("Accession " + accn.getAccessionName() + " does not belong to " + institute.getCode());
			}

			accessionRepository.delete(toDelete);

			for (Accession accession : toDelete) {
				AccessionHistoric hist = new AccessionHistoric();
				SelfCopy.copy(accession, hist);
				hist.setAccessionId(accessionIdRepository.findOne(accession.getId()));
				// hist.setId(hist.getAccessionId().getId());
				deleted.add(hist);
			}

			LOG.info("Done deleting, now adding");
			accessionHistoricRepository.save(deleted);
			// for (Accession a : toDelete) {
			// accessionRepository.deleteActive(a.getAccessionId().getId());
			// }
		}

		return deleted;
	}

	@Override
	// Worker threads don't carry this information
	// @PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<SvalbardData> saveSvalbards(List<SvalbardData> svalbards) {
		svalbardRepository.save(svalbards);
		return svalbards;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionCollect> saveCollecting(List<AccessionCollect> all) {
		accessionCollectRepository.save(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionCollect> removeCollecting(List<AccessionCollect> all) {
		accessionCollectRepository.delete(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionGeo> saveGeo(List<AccessionGeo> all) {
		accessionGeoRepository.save(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionGeo> removeGeo(List<AccessionGeo> all) {
		accessionGeoRepository.delete(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionBreeding> saveBreeding(List<AccessionBreeding> all) {
		accessionBreedingRepository.save(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionBreeding> removeBreeding(List<AccessionBreeding> all) {
		accessionBreedingRepository.delete(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionExchange> saveExchange(List<AccessionExchange> all) {
		accessionExchangeRepository.save(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionExchange> removeExchange(List<AccessionExchange> all) {
		accessionExchangeRepository.delete(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionRemark> saveRemarks(List<AccessionRemark> toSaveRemarks) {
		accessionRemarkRepository.save(toSaveRemarks);
		return toSaveRemarks;
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionRemark> removeRemarks(List<AccessionRemark> toRemoveRemarks) {
		accessionRemarkRepository.delete(toRemoveRemarks);
		return toRemoveRemarks;
	}

	@Override
	public long countAvailableForDistribution(Set<Long> accessionIds) {
		if (accessionIds == null || accessionIds.size() == 0) {
			return 0;
		}
		return accessionRepository.countAvailableForDistribution(accessionIds);
	}

	@Override
	public Set<Long> filterAvailableForDistribution(Set<Long> accessionIds) {
		if (accessionIds == null || accessionIds.size() == 0) {
			return Collections.emptySet();
		}
		return accessionRepository.filterAvailableForDistribution(accessionIds);
	}

	/**
	 * Returns datasets to which current user has 'WRITE'
	 */
	@Override
	@PreAuthorize("isAuthenticated()")
	public List<Metadata> listMyMetadata() {
		final AuthUserDetails user = SecurityContextUtil.getAuthUser();
		final List<Long> oids = aclService.listIdentitiesForSid(Metadata.class, user, BasePermission.WRITE);
		LOG.info("Got " + oids.size() + " elements for " + user.getUsername());
		if (oids.size() == 0) {
			return null;
		}

		return metadataRepository.findByIds(oids);
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	@CacheEvict(value = "statistics", allEntries = true)
	public Metadata addDataset(FaoInstitute institute, String title, String description) {
		final Metadata metadata = new Metadata();
		metadata.setInstituteCode(institute.getCode());
		metadata.setTitle(title);
		metadata.setDescription(htmlSanitizer.sanitize(description));
		return metadataRepository.save(metadata);
	}

	@Override
	public Metadata getDataset(long id) {
		return metadataRepository.findOne(id);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#metadata, 'WRITE')")
	public void touch(@Param("metadata") Metadata metadata) {

	}

	/**
	 * @param metadata
	 * @param accession
	 * @param methodValues
	 */
	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#metadata, 'WRITE')")
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public void upsertAccessionData(Metadata metadata, AccessionId accession, Map<Long, List<Object>> methodValues) {
		// // Load all existing records for metadata+accession
		// List<AccessionTrait> existingEntries =
		// accessionTraitRepository.findByMetadataIdAndAccession(metadata.getId(),
		// accession);
		//
		// // Remove entries missing from map
		// Collection<AccessionTrait> forRemoval = new
		// ArrayList<AccessionTrait>(methodValues.size());
		// Collection<AccessionTrait> forAddition = new
		// ArrayList<AccessionTrait>(methodValues.size());
		//
		// for (AccessionTrait accessionTrait : existingEntries) {
		// LOG.debug("Existing trait " + accessionTrait);
		// if (methodValues.containsKey(accessionTrait.getMethodId())) {
		// // keep it
		// } else {
		// LOG.info("New data does not have methodId=" +
		// accessionTrait.getMethodId());
		// forRemoval.add(accessionTrait);
		// }
		// }
		// // Add new entries from map
		// for (final long methodId : methodValues.keySet()) {
		// if (null == CollectionUtils.find(existingEntries, new Predicate() {
		// @Override
		// public boolean evaluate(Object object) {
		// return ((AccessionTrait) object).getMethodId() == methodId;
		// }
		// })) {
		// // Create new AccessionTrait
		// AccessionTrait at = new AccessionTrait();
		// at.setAccession(accession);
		// at.setMetadataId(metadata.getId());
		// at.setMethodId(methodId);
		// forAddition.add(at);
		// }
		// }
		// accessionTraitRepository.save(forAddition);
		// accessionTraitRepository.delete(forRemoval);

		LOG.info("Upserting accession data " + accession);
		final Set<Long> methodsWithValues = new HashSet<Long>();

		// stick them to database
		for (final long methodId : methodValues.keySet()) {
			final Method method = methodRepository.findOne(methodId);
			// traitValueRepository.delete(metadata, accession, method);
			// traitValueRepository.insert(metadata, accession, method,
			// methodValues.get(methodId));
			LOG.info("Upserting accession data for method " + method.getId() + " accn=" + accession);
			final List<Object> withValues = traitValueRepository.upsert(metadata, accession, method, methodValues.get(methodId));
			if (withValues.size() > 0) {
				methodsWithValues.add(method.getId());
			}
		}
	}

	@Override
	public int countAccessions(AppliedFilters filters) {
		return genesysLowlevelRepository.countAccessions(filters);
	}

	@Override
	// TODO FIXME Need proper term URLs
	public void writeAccessions(AppliedFilters filters, OutputStream outputStream) throws IOException {
		// UTF8 is used for encoding entry names
		final ZipOutputStream zos = new ZipOutputStream(outputStream);
		zos.setComment("Genesys Accessions filter=" + filters);
		zos.flush();

		// Filter information
		final ZipEntry readmeEntry = new ZipEntry("README.txt");
		readmeEntry.setComment("Extra iformation");
		readmeEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(readmeEntry);
		writeREADME(filters, zos);
		zos.closeEntry();
		zos.flush();

		// Accessions
		final ZipEntry coreEntry = new ZipEntry("core.csv");
		coreEntry.setComment("Accession information");
		coreEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(coreEntry);
		writeAccessionsCore(filters, zos);
		zos.closeEntry();
		zos.flush();

		// AccessionGeo
		ZipEntry entry = new ZipEntry("names.csv");
		entry.setComment("Accession Names");
		entry.setTime(System.currentTimeMillis());
		zos.putNextEntry(entry);
		writeAccessionsNames(filters, zos);
		zos.closeEntry();
		zos.flush();

		// AccessionGeo
		entry = new ZipEntry("geo.csv");
		entry.setComment("GIS information");
		entry.setTime(System.currentTimeMillis());
		zos.putNextEntry(entry);
		writeAccessionsGeo(filters, zos);
		zos.closeEntry();
		zos.flush();

		// AccessionCollect
		entry = new ZipEntry("coll.csv");
		entry.setComment("Collecting information");
		entry.setTime(System.currentTimeMillis());
		zos.putNextEntry(entry);
		writeAccessionsColl(filters, zos);
		zos.closeEntry();
		zos.flush();

		final ZipEntry metaEntry = new ZipEntry("meta.xml");
		metaEntry.setComment("Darwin Core Archive metadata");
		metaEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(metaEntry);
		final BufferedWriter osw = new BufferedWriter(new OutputStreamWriter(zos));
		osw.write("<?xml version='1.0' encoding='utf-8'?>\n");
		osw.write("<archive xmlns=\"http://rs.tdwg.org/dwc/text/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://rs.tdwg.org/dwc/text/ http://rs.tdwg.org/dwc/text/tdwg_dwc_text.xsd\">\n");
		osw.write("<core encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>core.csv</location></files>\n");
		osw.write("\t<id index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"UUID\"/>\n");
		osw.write("\t<field index=\"2\" term=\"http://purl.org/germplasm/germplasmType#wiewsInstituteID\"/>\n");
		osw.write("\t<field index=\"3\" term=\"http://purl.org/germplasm/germplasmTerm#germplasmIdentifier\"/>\n");
		osw.write("\t<field index=\"4\" term=\"http://rs.tdwg.org/dwc/terms/genus\"/>\n");
		osw.write("\t<field index=\"5\" term=\"http://rs.tdwg.org/dwc/terms/species\"/>\n");
		osw.write("\t<field index=\"6\" term=\"orgCty\"/>\n");
		osw.write("\t<field index=\"7\" term=\"acqSrc\"/>\n");
		osw.write("\t<field index=\"8\" term=\"acqDate\"/>\n");
		osw.write("\t<field index=\"9\" term=\"mlsStat\"/>\n");
		osw.write("\t<field index=\"10\" term=\"available\"/>\n");
		osw.write("\t<field index=\"11\" term=\"storage\"/>\n");
		osw.write("\t<field index=\"12\" term=\"sampStat\"/>\n");
		osw.write("\t<field index=\"13\" term=\"dublInst\"/>\n");
		osw.write("\t<field index=\"14\" term=\"createdBy\"/>\n");
		osw.write("\t<field index=\"15\" term=\"createdDate\"/>\n");
		osw.write("\t<field index=\"16\" term=\"lastModifiedBy\"/>\n");
		osw.write("\t<field index=\"17\" term=\"lastModifiedDate\"/>\n");
		osw.write("</core>\n");

		osw.write("<extension encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>");
		osw.write("names.csv");
		osw.write("</location></files>\n");
		osw.write("\t<coreid index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"instCode\"/>\n");
		osw.write("\t<field index=\"2\" term=\"name\"/>\n");
		osw.write("\t<field index=\"3\" term=\"aliasType\"/>\n");
		osw.write("\t<field index=\"4\" term=\"lang\"/>\n");
		osw.write("\t<field index=\"5\" term=\"version\"/>\n");
		osw.write("</extension>\n");

		osw.write("<extension encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>");
		osw.write("geo.csv");
		osw.write("</location></files>\n");
		osw.write("\t<coreid index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"latitude\"/>\n");
		osw.write("\t<field index=\"2\" term=\"longitude\"/>\n");
		osw.write("\t<field index=\"3\" term=\"elevation\"/>\n");
		osw.write("\t<field index=\"4\" term=\"datum\"/>\n");
		osw.write("\t<field index=\"5\" term=\"uncertainty\"/>\n");
		osw.write("\t<field index=\"6\" term=\"method\"/>\n");
		osw.write("\t<field index=\"7\" term=\"version\"/>\n");
		osw.write("</extension>\n");

		osw.write("<extension encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>");
		osw.write("coll.csv");
		osw.write("</location></files>\n");
		osw.write("\t<coreid index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"collMissId\"/>\n");
		osw.write("\t<field index=\"2\" term=\"collNumb\"/>\n");
		osw.write("\t<field index=\"3\" term=\"collDate\"/>\n");
		osw.write("\t<field index=\"4\" term=\"collSrc\"/>\n");
		osw.write("\t<field index=\"5\" term=\"collSite\"/>\n");
		osw.write("\t<field index=\"6\" term=\"collCode\"/>\n");
		osw.write("\t<field index=\"7\" term=\"collName\"/>\n");
		osw.write("\t<field index=\"8\" term=\"collInstAddress\"/>\n");
		osw.write("\t<field index=\"9\" term=\"version\"/>\n");
		osw.write("</extension>\n");

		osw.write("</archive>\n");
		osw.flush();
		zos.closeEntry();

		zos.finish();
	}

	private void writeREADME(AppliedFilters filters, ZipOutputStream zos) throws IOException {
		final BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(zos));
		bw.write("Date:\t");
		bw.write(new Date().toString());
		bw.write("\n");

		bw.write("Filter:\t");
		bw.write(filters.toString());
		bw.write("\n");

		bw.write("URL:\thttps://www.genesys-pgr.org/explore?filter=");
		bw.write(filters.toString());
		bw.write("\n");

		bw.write("Attribution:\t");
		bw.write("https://www.genesys-pgr.org/content/terms");
		bw.write("\n");
		bw.flush();
	}

	private void writeAccessionsCore(final AppliedFilters filters, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "uuid", "instCode", "acceNumb", "genus", "species", "fullTaxa", "orgCty", "acqSrc", "acqDate", "mlsStat",
				"available", "historic", "storage", "sampStat", "duplSite", "createdBy", "createdDate", "lastModifiedBy", "lastModifiedDate" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listAccessions(filters, new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					csv.writeNext(csvResultsetHelper.getColumnValues(rs));
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA", e);
				}
				if (i++ % 5000 == 0) {
					LOG.info("Writing accessions DWCA " + filters.toString() + " " + i);
				}
			}
		});

		csv.flush();
	}

	private void writeAccessionsGeo(final AppliedFilters filters, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "latitude", "longitude", "elevation", "datum", "uncertainty", "method", "version" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listAccessionsGeo(filters, new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					csv.writeNext(csvResultsetHelper.getColumnValues(rs));
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA", e);
				}
				if (i++ % 5000 == 0) {
					LOG.info("Writing geo DWCA " + filters.toString() + " " + i);
				}
			}
		});

		csv.flush();
	}

	private void writeAccessionsNames(final AppliedFilters filters, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "instCode", "name", "aliasType", "lang", "version" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listAccessionsAlias(filters, new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					final String[] r = csvResultsetHelper.getColumnValues(rs);
					if (r[3] != null) {
						try {
							final AliasType aliasType = AliasType.getType(Integer.parseInt(r[3]));
							r[3] = aliasType.name();
						} catch (final NumberFormatException e) {
							LOG.warn(e.getMessage());
						}
					}
					csv.writeNext(r);
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA aliases ", e);
				}
				if (i++ % 5000 == 0) {
					LOG.info("Writing alias DWCA " + filters.toString() + " " + i);
				}
			}
		});

		csv.flush();
	}

	private void writeAccessionsColl(final AppliedFilters filters, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "collMissId", "collNumb", "collDate", "collSrc", "collSite", "collCode", "collName", "collInstAddress",
				"version" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listAccessionsColl(filters, new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					csv.writeNext(csvResultsetHelper.getColumnValues(rs));
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA", e);
				}
				if (i++ % 5000 == 0) {
					LOG.info("Writing collecting DWCA " + filters.toString() + " " + i);
				}
			}
		});

		csv.flush();
	}

	@Override
	public void writeDataset(Metadata metadata, OutputStream outputStream) throws IOException {
		// Methods: A CSV is generated for each method
		final List<Method> metadataMethods = listMethods(metadata);

		CSVWriter csv = null;

		// UTF8 is used for encoding entry names
		final ZipOutputStream zos = new ZipOutputStream(outputStream);
		zos.setComment("Genesys Dataset " + metadata.getUuid());

		final ZipEntry coreEntry = new ZipEntry("core.csv");
		coreEntry.setComment("Accession information");
		coreEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(coreEntry);

		// Write accession information
		writeDatasetCore(metadata, zos);

		zos.closeEntry();

		for (final Method method : metadataMethods) {
			final ZipEntry methodEntry = new ZipEntry(String.format("%1$s.csv", method.getFieldName().toLowerCase()));
			methodEntry.setComment(method.getMethod());
			methodEntry.setTime(System.currentTimeMillis());
			zos.putNextEntry(methodEntry);

			final List<ExperimentAccessionTrait> vals = traitValueRepository.getValues(metadata, method);
			csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
			csv.writeNext(new String[] { "genesysId", "datasetId", "value" });

			Map<String, String> codeMap = null;
			if (method.isCoded()) {
				codeMap = TraitCode.parseCodeMap(method.getOptions());
			}
			for (final ExperimentAccessionTrait et : vals) {
				if (et.getValue() == null) {
					continue;
				}
				if (codeMap == null) {
					csv.writeNext(new String[] { String.valueOf(et.getAccessionId()), String.valueOf(et.getExperimentId()), et.getValue().toString() });
				} else {
					csv.writeNext(new String[] { String.valueOf(et.getAccessionId()), String.valueOf(et.getExperimentId()), codeMap.get(et.getValue()) });
				}
			}

			csv.flush();
			zos.closeEntry();
		}

		final ZipEntry metaEntry = new ZipEntry("meta.xml");
		metaEntry.setComment("Darwin Core Archive metadata");
		metaEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(metaEntry);
		final BufferedWriter osw = new BufferedWriter(new OutputStreamWriter(zos));
		osw.write("<?xml version='1.0' encoding='utf-8'?>\n");
		osw.write("<archive xmlns=\"http://rs.tdwg.org/dwc/text/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://rs.tdwg.org/dwc/text/ http://rs.tdwg.org/dwc/text/tdwg_dwc_text.xsd\">\n");
		osw.write("<core encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>core.csv</location></files>\n");
		osw.write("\t<id index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"http://purl.org/germplasm/germplasmType#wiewsInstituteID\"/>\n");
		osw.write("\t<field index=\"2\" term=\"http://purl.org/germplasm/germplasmTerm#germplasmIdentifier\"/>\n");
		osw.write("\t<field index=\"3\" term=\"http://rs.tdwg.org/dwc/terms/genus\"/>\n");
		osw.write("</core>\n");

		for (int i = 0; i < metadataMethods.size(); i++) {
			final Method method = metadataMethods.get(i);

			osw.write("<extension encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
			osw.write("\t<files><location>");
			osw.write(method.getFieldName().toLowerCase());
			osw.write(".csv</location></files>\n");
			osw.write("\t<coreid index=\"0\" />\n");
			osw.write("\t<field index=\"1\" term=\"http://rs.tdwg.org/dwc/terms/datasetID\"/>\n");
			osw.write("\t<field index=\"2\" term=\"http://www.genesys-pgr.org/descriptors//");
			osw.write(String.valueOf(method.getId()));
			osw.write("\"/>\n");
			osw.write("</extension>\n");
		}
		osw.write("</archive>\n");
		osw.flush();
		zos.closeEntry();

		zos.finish();
	}

	private void writeDatasetCore(final Metadata metadata, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos), 4096), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "instCode", "acceNumb", "genus" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listMetadataAccessions(metadata.getId(), new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					csv.writeNext(csvResultsetHelper.getColumnValues(rs));
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA", e);
				}
				if (i++ % 5000 == 0) {
					try {
						csv.flush();
					} catch (final IOException e) {
						throw new SQLException("Could not flush stream", e);
					}
					LOG.info("Writing metadata core DWCA " + metadata.getId() + " " + i);
				}
			}
		});

		csv.flush();
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@CacheEvict(value = "statistics", allEntries = true)
	public void refreshMetadataMethods() {
		genesysLowlevelRepository.refreshMetadataMethods();
	}

	@Override
	@Transactional
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionAlias> saveAliases(List<AccessionAlias> aliases) {
		if (aliases.size() > 0) {
			accessionAliasRepository.save(aliases);
		}
		return aliases;
	}

	@Override
	@Transactional
	@CacheEvict(value = "statistics", allEntries = true)
	public List<AccessionAlias> removeAliases(List<AccessionAlias> aliases) {
		if (aliases.size() > 0) {
			accessionAliasRepository.delete(aliases);
		}
		return aliases;
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@CacheEvict(value = "statistics", allEntries = true)
	public void upsertAliases(long accessionId, String acceNames, String otherIds) {
		final AccessionData accession = getAccession(accessionId);
		if (accession == null) {
			LOG.error("No such accession " + accessionId);
			return;
		}

		// LOG.info("Updating acceNames=" + acceNames);
		final List<AccessionAlias> aliases = new ArrayList<AccessionAlias>();
		if (StringUtils.isNotBlank(acceNames)) {
			final String[] acceName = acceNames.split(";");
			for (final String oi : acceName) {
				if (StringUtils.isBlank(oi)) {
					continue;
				}
				final AccessionAlias alias = new AccessionAlias();
				alias.setAccession(accession.getAccessionId());
				alias.setName(oi.trim());
				alias.setAliasType(AliasType.ACCENAME);
				aliases.add(alias);
			}
		}
		ensureAliases(accession, aliases, AliasType.ACCENAME);

		// LOG.info("Updating otherIds=" + otherIds);
		aliases.clear();
		if (StringUtils.isNotBlank(otherIds)) {
			final String[] otherId = otherIds.split(";");
			for (final String oi : otherId) {
				if (StringUtils.isBlank(oi)) {
					continue;
				}
				final String[] oin = oi.split(":", 2);
				final AccessionAlias alias = new AccessionAlias();
				alias.setAccession(accession.getAccessionId());
				if (oin.length == 1) {
					if (StringUtils.isBlank(oin[0])) {
						continue;
					}
					alias.setName(oin[0].trim());
				} else {
					if (StringUtils.isBlank(oin[1])) {
						continue;
					}
					alias.setUsedBy(StringUtils.defaultIfBlank(oin[0].trim(), null));

					if (alias.getUsedBy() != null && alias.getUsedBy().length() > 7) {
						LOG.warn("Invalid instCode: " + alias.getUsedBy() + " in=" + oi);
						continue;
					}

					alias.setName(oin[1].trim());
				}
				alias.setAliasType(AliasType.OTHERNUMB);
				aliases.add(alias);
			}
		}
		ensureAliases(accession, aliases, AliasType.OTHERNUMB);
	}

	private void ensureAliases(AccessionData accession, List<AccessionAlias> aliases, AliasType aliasType) {
		final List<AccessionAlias> existingAliases = accessionAliasRepository.findByAccessionAndAliasType(accession.getAccessionId(), aliasType.getId());

		// Find aliases to remove
		for (final AccessionAlias aa : existingAliases) {
			if (null == CollectionUtils.find(aliases, new Predicate<AccessionAlias>() {
				@Override
				public boolean evaluate(AccessionAlias alias) {
					return StringUtils.equals(alias.getName(), aa.getName());
				}
			})) {
				accessionAliasRepository.delete(aa);
			}
		}
		// Add or update
		for (final AccessionAlias aa : aliases) {
			final AccessionAlias accessionAlias = CollectionUtils.find(existingAliases, new Predicate<AccessionAlias>() {
				@Override
				public boolean evaluate(AccessionAlias alias) {
					return StringUtils.equals(alias.getName(), aa.getName());
				}
			});

			if (accessionAlias == null) {
				accessionAliasRepository.save(aa);
			}
		}
	}

	@Override
	@Transactional
	@CacheEvict(value = "statistics", allEntries = true)
	public Set<Long> removeAliases(Set<Long> toRemove) {
		for (final Long id : toRemove) {
			this.accessionAliasRepository.delete(id);
		}
		return toRemove;
	}

	@Override
	public List<Long> listAccessionsIds(Taxonomy2 taxonomy) {
		return accessionRepository.listAccessionsIds(taxonomy);
	}

	@Override
	@Transactional
	public int assignMissingUuid(int count) {
		List<AccessionId> accessionIds = accessionIdRepository.findMissingUuid(new PageRequest(0, count));
		for (AccessionId accessionId : accessionIds) {
			if (accessionId.getUuid() == null) {
				accessionId.setUuid(UUID.randomUUID());
			}
		}
		accessionIdRepository.save(accessionIds);
		LOG.info("Generated " + accessionIds.size() + " new UUIDs");
		return accessionIds.size();
	}

	@Override
	public AccessionHistoric getHistoricAccession(UUID uuid) {
		AccessionHistoric ah = accessionHistoricRepository.findOneByUuid(uuid);
		if (ah != null) {
			ah.getStoRage().size();
		}
		return ah;
	}

	@Override
	public PDCI loadPDCI(Long accessionId) {
		return repoPdci.findByAccessionId(accessionId);
	}

	@Override
	public List<PDCI> loadPDCI(List<Long> accessionIds) {
		return repoPdci.findByAccessionId(accessionIds);
	}

	@Override
	@Transactional
	@CacheEvict(value = "statistics", allEntries = true)
	public int generateMissingPDCI(int count) {
		List<AccessionId> accessionIds = accessionIdRepository.findMissingPDCI(new PageRequest(0, count));
		Set<Long> ids = new HashSet<Long>();
		for (AccessionId id : accessionIds) {
			if (id != null) {
				ids.add(id.getId());
			}
		}
		Set<AccessionDetails> ads = getAccessionDetails(ids);
		List<PDCI> pdcis = new ArrayList<PDCI>();
		for (AccessionDetails ad : ads) {
			PDCI pdci = new PDCI();
			AccessionId accessionId = null;
			for (AccessionId id : accessionIds) {
				if (id.getId().equals(ad.getId())) {
					accessionId = id;
				}
			}
			pdci.setAccession(accessionId);
			pdciCalculator.updatePdci(pdci, ad);
			pdcis.add(pdci);
		}
		LOG.info("Generated " + accessionIds.size() + " new PDCIs");
		repoPdci.save(pdcis);
		return pdcis.size();
	}

	@Override
	@Transactional
	@CacheEvict(value = "statistics", allEntries = true)
	public PDCI updatePDCI(Long accessionId) {
		if (pdciCalculator == null) {
			return null;
		}

		PDCI pdci = repoPdci.findByAccessionId(accessionId);
		if (pdci == null) {
			pdci = new PDCI();
			pdci.setAccession(accessionIdRepository.findOne(accessionId));
		}
		return repoPdci.save(pdciCalculator.updatePdci(pdci, accessionId));
	}

	@Override
	@Transactional
	@CacheEvict(value = "statistics", allEntries = true)
	public void updatePDCI(Set<AccessionDetails> ads) {
		if (pdciCalculator == null) {
			return;
		}
		Set<Long> accessionIds = new HashSet<Long>();
		for (AccessionDetails accessionDetails : ads) {
			if (accessionDetails != null) {
				accessionIds.add(accessionDetails.getId());
			}
		}
		List<PDCI> pdcis = new ArrayList<PDCI>();
		pdcis.addAll(repoPdci.findByAccessionId(accessionIds));

		for (AccessionDetails accessionDetails : ads) {
			if (accessionDetails == null) {
				continue;
			}

			PDCI pdci = null;
			for (PDCI existing : pdcis) {
				if (existing.getAccession().getId().equals(accessionDetails.getId())) {
					pdci = existing;
					break;
				}
			}
			if (pdci == null) {
				pdci = new PDCI();
				pdci.setAccession(accessionIdRepository.findOne(accessionDetails.getId()));
				pdcis.add(pdci);
			}
			pdciCalculator.updatePdci(pdci, accessionDetails);
		}
		repoPdci.save(pdcis);
	}

	@Override
	public PDCIStatistics statisticsPDCI(FaoInstitute faoInstitute) {
		PDCIStatistics stats = new PDCIStatistics();

		Object[] overall = (Object[]) repoPdci.statistics(faoInstitute);
		if (overall == null) {
			LOG.warn("No PDCI statistics for " + faoInstitute);
			return null;
		}

		// Determining JPA data types
		// System.err.println(overall[0].getClass());
		// System.err.println(overall[1].getClass());
		// System.err.println(overall[2].getClass());
		// System.err.println(overall[3].getClass());

		stats.setMin((Float) overall[0]);
		stats.setMax((Float) overall[1]);
		stats.setAvg((Double) overall[2]);
		stats.setCount((Long) overall[3]);

		List<Object[]> hist = repoPdci.histogram(faoInstitute);
		stats.makeHistogram(hist);

		return stats;
	}

	@Override
	public PDCIStatistics statisticsPDCI(Organization organization) {
		PDCIStatistics stats = new PDCIStatistics();

		for (FaoInstitute faoInstitute : organizationService.getMembers(organization)) {
			Object[] overall = (Object[]) repoPdci.statistics(faoInstitute);
			if (overall == null) {
				LOG.warn("No PDCI statistics for " + organization);
				continue;
			}

			stats.updateMin((Float) overall[0]);
			stats.updateMax((Float) overall[1]);
			stats.updateCountAndAvg((Long) overall[3], (Double) overall[2]);

			List<Object[]> hist = repoPdci.histogram(faoInstitute);
			stats.makeHistogram(hist);
		}
		return stats;
	}

	@Override
	public PhenoStatistics statisticsPheno(FaoInstitute faoInstitute) {
		List<Object[]> metadatas = accessionTraitRepository.statisticsAccessionMetadata(faoInstitute);
		List<Object[]> methods = accessionTraitRepository.statisticsAccessionMethod(faoInstitute);
		long count = countByInstitute(faoInstitute);

		PhenoStatistics stats = new PhenoStatistics();
		stats.setCount(count);

		{
			long withMetadata = 0, metadatasTotal = 0;
			for (Object[] m : metadatas) {
				// Long accessionId = (Long) m[0];
				Long metadataCount = (Long) m[1];
				if (metadataCount != null && metadataCount > 0) {
					withMetadata++;
					metadatasTotal += metadataCount;
				}
			}
			stats.setWithMetadata(withMetadata);
			stats.setAvgMetadatas((float) metadatasTotal / withMetadata);
		}
		{
			long withMethod = 0, methodsTotal = 0;
			for (Object[] m : methods) {
				// Long accessionId = (Long) m[0];
				Long methodCount = (Long) m[1];
				if (methodCount != null && methodCount > 0) {
					withMethod++;
					methodsTotal += methodCount;
				}
			}
			stats.setWithMethod(withMethod);
			stats.setAvgMethods((float) methodsTotal / withMethod);
		}
		return stats;
	}
}
