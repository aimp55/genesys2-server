package org.genesys2.server.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSDescriptor;
import org.genesys2.server.model.dataset.DSQualifier;
import org.genesys2.server.model.dataset.DSRow;
import org.genesys2.server.model.dataset.DSRowQualifier;
import org.genesys2.server.model.dataset.DSValue;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.impl.Descriptor;
import org.genesys2.server.model.json.WorldclimJson;
import org.genesys2.server.persistence.domain.AccessionGeoRepository;
import org.genesys2.server.persistence.domain.DescriptorRepository;
import org.genesys2.server.persistence.domain.dataset.DSDescriptorRepository;
import org.genesys2.server.persistence.domain.dataset.DSQualifierRepository;
import org.genesys2.server.persistence.domain.dataset.DSRepository;
import org.genesys2.server.persistence.domain.dataset.DSRowQualifierRepository;
import org.genesys2.server.persistence.domain.dataset.DSRowRepository;
import org.genesys2.server.persistence.domain.dataset.DSValueRepository;
import org.genesys2.server.service.DSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class DSServiceImpl implements DSService {
	public static final Log LOG = LogFactory.getLog(DSServiceImpl.class);

	private static final Charset CHARSET_UTF8 = Charset.forName("UTF8");

	@Autowired
	private DSRepository dsRepo;
	@Autowired
	private DSDescriptorRepository dsDescRepo;
	@Autowired
	private DSQualifierRepository dsQualRepo;
	@Autowired
	private DSRowRepository dsRowRepo;
	@Autowired
	private DSRowQualifierRepository dsRowQualiRepo;
	@Autowired
	private DSValueRepository dsValueRepo;
	@Autowired
	private DescriptorRepository descriptorRepo;

	@Autowired
	private AccessionGeoRepository accessionGeoRepository;

	@Transactional
	@Override
	public void saveDataset(DS ds) {
		dsRepo.save(ds);
	}

	@Transactional
	@Override
	public DSQualifier addQualifier(DS ds, Descriptor d1) {
		DSQualifier dsq = new DSQualifier();
		dsq.setDataset(ds);
		dsq.setDescriptor(d1);
		dsq.setIndex(ds.getQualifiers().size() + 1);
		ds.getQualifiers().add(dsq);
		return dsQualRepo.save(dsq);
	}

	@Transactional
	@Override
	public DSDescriptor addDescriptor(DS ds, Descriptor d1) {
		DSDescriptor dsd = new DSDescriptor();
		dsd.setDataset(ds);
		dsd.setDescriptor(d1);
		dsd.setIndex(ds.getDescriptors().size() + 1);
		ds.getDescriptors().add(dsd);
		return dsDescRepo.save(dsd);
	}

	@Override
	public DS loadDatasetByUuid(UUID uuid) {
		DS ds = dsRepo.findByUuid(uuid);
		if (ds != null) {
			ds.getQualifiers().size();
			ds.getDescriptors().size();
		}
		return ds;
	}

	/**
	 * 
	 */
	@Transactional
	@Override
	public void updateRow(DS ds, Object[] row) {
		List<DSQualifier> dsqs = ds.getQualifiers();
		LOG.info("DS.qualifiers: " + dsqs);
		List<DSDescriptor> dsds = ds.getDescriptors();
		LOG.info("DS.descriptors: " + dsds);

		Object[] qualifiers = new Object[dsqs.size()];
		System.arraycopy(row, 0, qualifiers, 0, qualifiers.length);
		Object[] values = new Object[dsds.size()];
		System.arraycopy(row, qualifiers.length, values, 0, values.length);

		DSRow dsr = dsRowRepo.findRow(ds, qualifiers);
		if (dsr == null) {
			dsr = new DSRow();
			dsr.setDataset(ds);
			dsRowRepo.save(dsr);
			LOG.info("Made new row " + dsr.getId());

			List<DSRowQualifier<?>> rowQualifiers = new ArrayList<DSRowQualifier<?>>();
			int i = 0;
			for (DSQualifier dsq : dsqs) {
				DSRowQualifier<?> dsrq = DSRowQualifier.make(qualifiers[i]);
				dsrq.setDatasetQualifier(dsq);
				dsrq.setRow(dsr);
				rowQualifiers.add(dsrq);
				i++;
			}
			dsRowQualiRepo.save(rowQualifiers);
			dsr.setRowQualifiers(rowQualifiers);
			dsr.setValues(new ArrayList<DSValue<?>>(values.length));

			// Test load row
			// DSRow testLoad = dsRowRepo.findRow(ds, qualifiers);
			// LOG.info("Loaded row " + testLoad.getId());
		}

		List<DSValue<?>> dsrvs = dsr.getValues();

		LOG.info("Row " + dsr + " has values.size=" + dsrvs.size());

		List<DSValue<?>> toSave = new ArrayList<DSValue<?>>();

		for (int i = 0; i < values.length; i++) {
			Object v = values[i];
			if (v == null) {
				continue;
			}

			DSValue<?> dsrv = null;
			DSDescriptor dsd = dsds.get(i);
			LOG.info("Looking for " + dsd);
			for (DSValue<?> dsrv1 : dsrvs) {
				if (dsd.getId().equals(dsrv1.getDatasetDescriptor().getId())) {
					LOG.info("Found matching dsd " + dsrv1.getDatasetDescriptor());
					dsrv = dsrv1;
					break;
				}
			}

			if (dsrv == null) {
				LOG.info("Making new value for row " + dsr + " and " + dsd);
				dsrv = DSValue.make(v);
				dsrv.setDatasetDescriptor(dsd);
				dsrv.setRow(dsr);
				dsr.getValues().add(dsrv);
			}
			dsrv.setValue2(v);
			toSave.add(dsrv);
		}

		dsValueRepo.save(toSave);
	}

	/**
	 * 
	 */
	@Transactional
	@Override
	public void updateRow(DS ds, Object[] qualifiers, DSDescriptor dsd, Object value) {
		if (value == null) {
			return;
		}

		DSRow dsr = findOrMakeRow(ds, qualifiers);
		saveRow(dsr);

		DSValue<?> dsrv = dsValueRepo.findByRowAndDatasetDescriptor(dsr, dsd);
		if (dsrv == null) {
			LOG.debug("Making new value for row " + dsr + " and " + dsd);
			dsrv = DSValue.make(value);
			dsrv.setDatasetDescriptor(dsd);
			dsrv.setRow(dsr);
		}
		dsrv.setValue2(value);
		dsValueRepo.save(dsrv);
	}

	private void saveRow(DSRow dsr) {
		dsRowRepo.save(dsr);
		dsRowQualiRepo.save(dsr.getRowQualifiers());
		if (LOG.isDebugEnabled()) {
			LOG.debug("Saved new row " + dsr.getId());
		}
	}

	private void saveRows(Collection<DSRow> dsrs) {
		dsRowRepo.save(dsrs);
		ArrayList<DSRowQualifier<?>> quali = new ArrayList<DSRowQualifier<?>>();
		for (DSRow dsr : dsrs) {
			quali.addAll(dsr.getRowQualifiers());
		}
		dsRowQualiRepo.save(quali);
	}

	private DSRow findOrMakeRow(DS ds, Object[] qualifiers) {
		DSRow dsr = null;
		if (qualifiers.length == 1) {
			dsr = dsRowRepo.findByLongQualifier(ds, (Long) qualifiers[0]);
		} else {
			dsr = dsRowRepo.findRow(ds, qualifiers);
		}
		if (dsr == null) {
			dsr = makeRow(ds, qualifiers);
		}
		return dsr;
	}

	private Map<Object, DSRow> findOrMakeRows(DS ds, Collection<?> qualifiers) {
		Map<Object, DSRow> dsrs = new HashMap<Object, DSRow>();
		Iterator<?> it = qualifiers.iterator();

		if (!it.hasNext()) {
			// Blank
			return dsrs;
		}
		Object sampleQualifier = it.next();

		if (sampleQualifier instanceof Object[]) {
			LOG.debug("Qualifiers are arrays!");
			// TODO Sort it out
			for (Object qs : qualifiers) {
				dsrs.put(qs, findOrMakeRow(ds, (Object[]) qs));
			}
		} else if (sampleQualifier instanceof Long) {
			LOG.debug("Qualifiers are Longs");
			List<Object[]> existing = dsRowRepo.findByLongQualifier(ds, qualifiers);

			Map<Long, DSRow> qualifierMap = new HashMap<Long, DSRow>();
			for (Object[] e : existing) {
				qualifierMap.put((Long) e[0], (DSRow) e[1]);
			}
			dsrs.putAll(qualifierMap);

			// Make missing rows
			Set<Long> missing = new HashSet<Long>();
			for (Object x : qualifiers) {
				missing.add((Long) x);
			}
			for (Long dsrq1id : qualifierMap.keySet()) {
				missing.remove(dsrq1id);
			}
			for (Long miss : missing) {
				// LOG.info("Adding missing row for " + miss);
				dsrs.put(miss, makeRow(ds, miss));
			}
		}

		return dsrs;
	}

	private DSRow makeRow(DS ds, Object... qualifiers) {
		DSRow dsr = new DSRow();

		ByteBuffer keyBuffer = ByteBuffer.allocate(500);

		List<DSRowQualifier<?>> rowQualifiers = new ArrayList<DSRowQualifier<?>>();
		int i = 0;
		for (DSQualifier dsq : ds.getQualifiers()) {
			DSRowQualifier<?> dsrq = DSRowQualifier.make(qualifiers[i]);
			if (qualifiers[i] instanceof Long) {
				keyBuffer.putLong(((Long) qualifiers[i]).longValue());
			} else if (qualifiers[i] instanceof String) {
				keyBuffer.put(((String) qualifiers[i]).getBytes(CHARSET_UTF8));
			} else if (qualifiers[i] instanceof Double) {
				keyBuffer.putDouble(((Double) qualifiers[i]).doubleValue());
			}
			dsrq.setDatasetQualifier(dsq);
			dsrq.setRow(dsr);
			rowQualifiers.add(dsrq);
			i++;
		}
		// LOG.info("MD4 length: " + DigestUtils.md5(keyBuffer.array()).length);
		// LOG.info("SHA1 length: " +
		// DigestUtils.sha1(keyBuffer.array()).length);
		dsr.setMd5(DigestUtils.md5(keyBuffer.array()));
		dsr.setSha1(DigestUtils.sha1(keyBuffer.array()));
		dsr.setDataset(ds);
		
		dsr.setRowQualifiers(rowQualifiers);
		return dsr;
	}

	@Transactional
	@Override
	public void updateRows(DS ds, List<Object[]> rows) {
		for (Object[] row : rows) {
			updateRow(ds, row);
		}
	}

	/**
	 * Updates worldclim data for accessions
	 * 
	 * @param tileIndexes
	 * @param buffer
	 * @param nullValue
	 */
	@Override
	@Transactional
	public void worldclimUpdate(DS dataset, DSDescriptor dsd, Set<Long> tileIndexes, MappedByteBuffer buffer, short nullValue, double factor) {
		if (LOG.isInfoEnabled()) {
			LOG.info("Updating " + dsd.getDescriptor().getCode() + " for tileIndexes: " + tileIndexes.size());
		}

		Map<Object, DSRow> rowMap = findOrMakeRows(dataset, tileIndexes);

		// Load all existing values for rows
		Map<Long, DSValue<?>> values = dsRowRepo.rowValueMap(rowMap.values(), dsd);
		List<DSValue<?>> toSave = new ArrayList<DSValue<?>>();

		for (final Long tileIndex : tileIndexes) {
			if (tileIndex * 2 > buffer.capacity() - 2) {
				LOG.info("OUT OF FILE tile=" + tileIndex);
			} else {
				try {
					short val = buffer.getShort((int) (tileIndex * 2));
					if (val != nullValue) {
						if (LOG.isDebugEnabled()) {
							LOG.debug("tile=" + tileIndex + " val=" + val);
						}

						Object value = null;
						if (factor < 1.0) {
							// FIXME Find a better way!
							value = new Double(factor * val);
						} else if (factor == 1.0) {
							value = new Long((long) (factor * val));
						}

						DSRow dsr = rowMap.get(tileIndex);
						DSValue<?> dsrv = null;

						if (dsr.getId() != null)
							dsrv = values.get(dsr.getId());

						if (dsrv == null) {
							LOG.debug("Making new value for row " + dsr + " and " + dsd);
							dsrv = DSValue.make(value);
							dsrv.setDatasetDescriptor(dsd);
							dsrv.setRow(dsr);
						}
						dsrv.setValue2(value);
						toSave.add(dsrv);
					}
				} catch (IndexOutOfBoundsException e) {
					LOG.error("OUT OF BOUND tile=" + tileIndex + " capacity=" + buffer.capacity() + " limit=" + buffer.limit());
					throw e;
				}
			}
		}

		// Only save new rows
		Collection<DSRow> newRows = new ArrayList<DSRow>();
		for (DSRow dsr : rowMap.values()) {
			if (dsr.getId() == null)
				newRows.add(dsr);
		}
		saveRows(newRows);

		// Save values
		dsValueRepo.save(toSave);

		LOG.info("Done saving.");
	}

	@Override
	@Transactional
	public void updateAccessionTileIndex(List<AccessionGeo> toSave) {
		accessionGeoRepository.save(toSave);
	}

	@Override
	public WorldclimJson jsonForTile(DS worldClimDataset, Long tileIndex) {
		DSRow dsr = dsRowRepo.findByLongQualifier(worldClimDataset, tileIndex);
		if (dsr == null || dsr.getValues().isEmpty())
			return null;

		WorldclimJson wc = new WorldclimJson();
		for (DSValue<?> v : dsr.getValues()) {
			String variable = v.getDatasetDescriptor().getDescriptor().getCode();
			Matcher matcher = Pattern.compile("(prec|tmin|tmean|tmax)(\\d{1,2})").matcher(variable);
			if (!matcher.matches()) {
				// Add to "Other"
				wc.addOther(v.getDatasetDescriptor().getDescriptor(), v.getValue());
				continue;
			}
			String varName = matcher.group(1);
			int month = Integer.parseInt(matcher.group(2));

			if ("prec".equals(varName)) {
				wc.getPrecipitation()[month - 1] = (Long) v.getValue();
			} else if ("tmin".equals(varName)) {
				wc.getTempMin()[month - 1] = (Double) v.getValue();
			} else if ("tmean".equals(varName)) {
				wc.getTempMean()[month - 1] = (Double) v.getValue();
			} else if ("tmax".equals(varName)) {
				wc.getTempMax()[month - 1] = (Double) v.getValue();
			}
		}
		return wc;
	}

	@Override
	public void download(DS ds, List<DSDescriptor> dsds, OutputStream outputStream) throws IOException {
		XSSFWorkbook template = new XSSFWorkbook();

		// keep 1000 rows in memory, exceeding rows will be flushed to disk
		SXSSFWorkbook wb = new SXSSFWorkbook(template, 100);

		SXSSFSheet sheet = (SXSSFSheet) wb.createSheet("worldclim.org");

		int rowIndex = 0, cellIndex = 0;
		Row r = sheet.createRow(rowIndex++);
		r.createCell(cellIndex++).setCellValue("row_id");
		for (DSQualifier dsq : ds.getQualifiers()) {
			r.createCell(cellIndex++).setCellValue(dsq.getDescriptor().getCode());
		}
		// Prepare descriptor mapping
		Long[] columnDescriptors = new Long[dsds.size()];
		{
			int columnIndex = 0;
			for (DSDescriptor dsd : dsds) {
				r.createCell(cellIndex++).setCellValue(dsd.getDescriptor().getCode());
				columnDescriptors[columnIndex++] = dsd.getId();
			}
		}

		// List<DSRow> rows = dsRowRepo.findByDataset(ds);
		// LOG.info("Retrieved DS rows " +rows.size());

		// rowId, dsdq1.value, dsdq2.value...
		List<Object[]> allDsrq = dsRowRepo.getQualifiers(ds);
		LOG.info("Got " + allDsrq.size() + " row qualifiers");

		int batchSize = 100;

		for (int fromIndex = 0; fromIndex < allDsrq.size(); fromIndex += batchSize) {
			List<Object[]> batch = allDsrq.subList(fromIndex, Math.min(fromIndex + batchSize, allDsrq.size()));
			LOG.info("Processing position: " + fromIndex + " of " + allDsrq.size());

			List<Long> rowIds = new ArrayList<Long>(batchSize);
			for (Object[] x : batch) {
				rowIds.add((Long) x[0]);
			}
			List<Object[]> allValues = dsRowRepo.getRowValues(rowIds, columnDescriptors);
			int batchPos = 0;
			for (Object[] x : batch) {
				r = sheet.createRow(rowIndex++);
				cellIndex = 0;
				for (Object v : x) {
					addCell(r, cellIndex++, v);
				}

				Object[] values = allValues.get(batchPos++);
				if (values != null) {
					for (Object v : values) {
						addCell(r, cellIndex++, v);
					}
				}
			}

			// if (rowIndex > 10000) {
			// LOG.warn("Breaking");
			// break;
			// }
		}
		sheet.flushRows();

		LOG.info("Writing to output stream");
		wb.write(outputStream);
		wb.dispose();
		LOG.info("Done");
	}

	@Override
	public void download(DS ds, OutputStream outputStream) throws IOException {
		download(ds, ds.getDescriptors(), outputStream);
	}

	private void addCell(Row r, int cellIndex, Object v) {
		if (v == null)
			return;
		Cell c = r.createCell(cellIndex);
		if (v instanceof String) {
			c.setCellValue((String) v);
		} else if (v instanceof Number) {
			c.setCellValue(((Number) v).doubleValue());
		} else if (v instanceof Date) {
			c.setCellValue((Date) v);
		}
	}

	@Override
	@Transactional
	public void deleteDataset(DS ds) {
		int count = dsQualRepo.deleteFor(ds);
		LOG.info("Deleted " + count + " DSQualifiers");
		dsRepo.delete(ds.getId());
		LOG.info("Deleted dataset " + ds.getUuid());
	}

	@Override
	@Transactional
	public void deleteRows(DS ds) {
		LOG.info("Clearing DSRowQuali for " + ds);
		int count = dsRowQualiRepo.deleteFor(ds);
		LOG.info("Deleted " + count + " DSRowQualifiers");
		count = dsRowRepo.deleteFor(ds);
		LOG.info("Deleted " + count + " DSRows");
	}

	@Override
	@Transactional
	public void deleteDescriptor(DSDescriptor dsd) {
		LOG.info("Clearing DSValues for " + dsd.getDescriptor().getCode());
		int count = dsValueRepo.deleteFor(dsd);
		LOG.info("Deleted " + count + " DSValue cells");
		dsDescRepo.delete(dsd);
	}
}
