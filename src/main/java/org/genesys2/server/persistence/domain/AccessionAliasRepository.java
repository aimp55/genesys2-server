/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.List;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionAlias;
import org.genesys2.server.model.genesys.AccessionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccessionAliasRepository extends JpaRepository<AccessionAlias, Long> {

	List<AccessionAlias> findByAccession(AccessionId accession);

	@Query("select aa from AccessionAlias aa where aa.accession = ?1 and aa.aliasType = ?2")
	List<AccessionAlias> findByAccessionAndAliasType(AccessionId accession, int aliasType);

	@Modifying
	@Query("delete from AccessionAlias aa where aa.accession.id in ( :ids )")
	void deleteForAccessions(@Param("ids") Collection<Long> accessionIds);

	@Query("select distinct a from Accession a where a.accessionId in (select distinct aa.accession from AccessionAlias aa where aa.usedBy=?1 and aa.name=?2 and aa.aliasType=?3) and a.institute.code=?1")
	Accession findAccession(String instCode, String name, int aliasType);

	@Query("from AccessionAlias aa where aa.accession.id in ?1")
	List<AccessionAlias> findAllFor(Collection<Long> accessionIds);
}
