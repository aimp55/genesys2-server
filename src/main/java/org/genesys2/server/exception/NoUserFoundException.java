/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.exception;

public class NoUserFoundException extends UserException {

	
	private static final long serialVersionUID = -3218537334751840421L;

	public NoUserFoundException() {
	}

	public NoUserFoundException(String message) {
		super(message);
	}

	public NoUserFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoUserFoundException(Throwable cause) {
		super(cause);
	}

	public NoUserFoundException(long modelId) {
		this.modelId = modelId;
	}

	public NoUserFoundException(Throwable cause, long modelId) {
		super(cause);
		this.modelId = modelId;
	}

	private long modelId;

	public long getModelId() {
		return modelId;
	}

}
