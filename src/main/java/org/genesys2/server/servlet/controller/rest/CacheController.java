/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.monitor.LocalMapStats;
import org.genesys2.server.service.MappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;


@Controller("restCacheController")
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0/cache", "/json/v0/cache" })
public class CacheController extends RestController {

	@Autowired
	private MappingService mappingService;

	@Autowired
	private CacheManager cacheManager;

	@RequestMapping(value = "", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Object cacheStats() {
		List<CacheStats> cacheMaps = new ArrayList<>();
		List<Object> cacheOther = new ArrayList<>();
		HashMap<String, Object> response = new HashMap<>();

		Set<HazelcastInstance> instances = Hazelcast.getAllHazelcastInstances();
		for (HazelcastInstance hz : instances) {
			if (LOG.isDebugEnabled())
				LOG.debug("\n\nCache stats Instance: " + hz.getName());
			for (DistributedObject o : hz.getDistributedObjects()) {
				if (o instanceof IMap) {
					IMap imap = (IMap) o;
					cacheMaps.add(new CacheStats(imap));
				} else {
//					cacheOther.add(o); //todo, error when transforming to json
					LOG.info(o.getClass() + " " + o);
				}
			}
		}

		response.put("cacheMaps", cacheMaps);
		response.put("cacheOther", cacheOther);

		return response;
	}

	@RequestMapping( value = "/clearTilesCache", method = RequestMethod.POST,  produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Object clearTilesCache() {
		final Cache tileServerCache = cacheManager.getCache("tileserver");
		System.err.println("tileServerCache=" + tileServerCache.getNativeCache());

		@SuppressWarnings("rawtypes")
		final IMap hazelCache = (IMap) tileServerCache.getNativeCache();

		LOG.info("Tiles cache size=" + hazelCache.size());
		int count = 0;
		for (final Object key : hazelCache.keySet()) {
			LOG.info("\tkey=" + key);
			if (++count > 20) {
				break;
			}
		}
		mappingService.clearCache();
		LOG.info("Tiles cache size=" + hazelCache.size());

		return JSON_OK;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/clearCaches")
	public @ResponseBody Object clearCaches() {
		for (String cacheName : cacheManager.getCacheNames()) {
			final Cache cache = cacheManager.getCache(cacheName);
			LOG.info("Clearing cache " + cacheName);
			cache.clear();
		}
		return JSON_OK;
	}


	public static final class CacheStats {

		private String serviceName;
		private String name;
		private LocalMapStats mapStats;

		public CacheStats(IMap<?, ?> imap) {
			this.serviceName = imap.getServiceName();
			this.name = imap.getName();
			this.mapStats = imap.getLocalMapStats();
		}

		public String getServiceName() {
			return serviceName;
		}

		public String getName() {
			return name;
		}

		public LocalMapStats getMapStats() {
			return mapStats;
		}
	}
}
