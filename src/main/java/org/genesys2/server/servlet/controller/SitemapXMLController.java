/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * http://www.sitemaps.org/protocol.html
 * 
 * @author Matija Obreza, matija.obreza@croptrust.org
 */
@Controller
public class SitemapXMLController {

	@Value("${base.url}")
	private String baseUrl;

	@Autowired
	private GeoService geoService;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private CropService cropService;

	private static class SitemapPage {
		String url;
		String freq = "always";
		Double priority;

		public SitemapPage(String url) {
			this.url = url;
		}

		public SitemapPage(String url, double priority) {
			this.url = url;
			this.priority = priority;
		}

		public SitemapPage(String url, String changeFrequency, double priority) {
			this.url = url;
			this.freq = changeFrequency;
			this.priority = priority;
		}

	}

	private final static SitemapPage[] sitemaps = new SitemapPage[] { new SitemapPage("/sitemap-content.xml"), new SitemapPage("/sitemap-geo.xml"),
			new SitemapPage("/sitemap-wiews.xml"), new SitemapPage("/sitemap-crop.xml") };

	private final SitemapPage[] sitemapPages = new SitemapPage[] { new SitemapPage("/welcome", 1.0), new SitemapPage("/content/about", "monthly", 0.2),
			new SitemapPage("/explore/overview", 1.0), new SitemapPage("/wiews/active", 1.0), new SitemapPage("/content/terms", 1.0),
			new SitemapPage("/explore", 1.0), new SitemapPage("/org/") };

	@RequestMapping(value = "/sitemap.xml", method = RequestMethod.GET)
	public @ResponseBody
	String sitemapsXml(HttpServletResponse response) {
		response.setContentType("text/xml");

		final StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");
		for (final SitemapPage page : sitemaps) {
			sb.append(" <sitemap>");
			sb.append("   <loc>").append(baseUrl).append(response.encodeURL(page.url)).append("</loc>");
			sb.append(" </sitemap>");
		}
		sb.append("</sitemapindex>");
		return sb.toString();

	}

	@RequestMapping(value = "/sitemap-content.xml", method = RequestMethod.GET)
	public @ResponseBody
	String sitemapContentXml(HttpServletResponse response) {
		return writeSitemap(response, sitemapPages);
	}

	@RequestMapping(value = "/sitemap-geo.xml", method = RequestMethod.GET)
	public @ResponseBody
	String sitemapGeoXml(HttpServletResponse response) {
		response.setContentType("text/xml");

		final StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");

		List<Country> activeCountries = geoService.listActive(LocaleContextHolder.getLocale());
		for (Country country : activeCountries) {
			writePage(response, sb, new SitemapPage("/geo/" + country.getCode3().toUpperCase()));
		}

		sb.append("</urlset>");
		return sb.toString();
	}

	@RequestMapping(value = "/sitemap-wiews.xml", method = RequestMethod.GET)
	public @ResponseBody
	String sitemapWiewsXml(HttpServletResponse response) {
		response.setContentType("text/xml");

		final StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");

		for (FaoInstitute institute : instituteService.listActive(new PageRequest(0, Integer.MAX_VALUE))) {
			writePage(response, sb, new SitemapPage("/wiews/" + institute.getCode().toUpperCase()));
		}

		sb.append("</urlset>");
		return sb.toString();
	}

	@RequestMapping(value = "/sitemap-crop.xml", method = RequestMethod.GET)
	public @ResponseBody
	String sitemapCropXml(HttpServletResponse response) {
		response.setContentType("text/xml");

		final StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");

		for (Crop crop : cropService.listCrops()) {
			writePage(response, sb, new SitemapPage("/c/" + crop.getShortName() + "/data"));
		}

		sb.append("</urlset>");
		return sb.toString();
	}

	private String writeSitemap(HttpServletResponse response, SitemapPage[] pages) {
		response.setContentType("text/xml");

		final StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");
		for (final SitemapPage page : pages) {
			writePage(response, sb, page);
		}
		sb.append("</urlset>");
		return sb.toString();
	}

	private void writePage(HttpServletResponse response, final StringBuffer sb, final SitemapPage page) {
		sb.append(" <url>");
		sb.append(" <loc>").append(baseUrl).append(response.encodeURL(page.url)).append("</loc>");
		if (page.freq != null) {
			sb.append(" <changefreq>").append(page.freq).append("</changefreq>");
		}
		if (page.priority != null) {
			sb.append(" <priority>").append(page.priority).append("</priority>");
		}
		sb.append(" </url>");
	}
}
