/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.ValidationException;

import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.MinLength;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import org.genesys2.server.exception.AuthorizationException;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.impl.AccessionIdentifier3;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.json.Api1Constants;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.DatasetService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller("restDatasetController")
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0/datasets", "/json/v0/datasets" })
public class DatasetController extends RestController {

	@Autowired
	DatasetService datasetService;

	@Autowired
	GenesysService genesysService;

	@Autowired
	TraitService traitService;

	@Autowired
	CropService cropService;

	@Autowired
	InstituteService instituteService;

	/**
	 * List all crops
	 *
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object listDatasets() throws AuthorizationException {
		LOG.info("Listing datasets");
		final List<Metadata> datasets = datasetService.listMyMetadata();
		return OAuth2Cleanup.clean(datasets);
	}

	/**
	 * Add a crop
	 *
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object createDataset(@RequestBody MetadataJson metadataJson) throws ValidationException {
		LOG.info("Creating metadata");
		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(metadataJson);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Validation problem", violations);
		}
		final FaoInstitute faoInstitute = instituteService.getInstitute(metadataJson.institute);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}

		final Metadata metadata = datasetService.addDataset(faoInstitute, metadataJson.title, metadataJson.description);
		return metadata;
	}

	/**
	 * Get metadata details /datasets/{shortName}
	 *
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{metadataId:.+}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object getCrop(@PathVariable("metadataId") Long metadataId) throws AuthorizationException {
		LOG.info("Getting metadata " + metadataId);
		return OAuth2Cleanup.clean(datasetService.getDataset(metadataId));
	}

	/**
	 * Get metadata Methods /datasets/{shortName}/methods
	 *
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{metadataId:.+}/methods", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object getMetadataMethods(@PathVariable("metadataId") Long metadataId) throws AuthorizationException {
		LOG.info("Getting metadata methods for " + metadataId);
		final Metadata metadata = datasetService.getDataset(metadataId);
		if (metadata == null) {
			throw new ResourceNotFoundException();
		}
		return OAuth2Cleanup.clean(genesysService.listMethods(metadata));
	}

	/**
	 * Get metadata Methods /datasets/{shortName}/methods
	 *
	 * @return
	 * @throws AuthorizationException
	 * @throws IOException
	 * @throws NonUniqueAccessionException 
	 */
	@RequestMapping(value = "/{metadataId:.+}/data", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object upsertMetadataData(@PathVariable("metadataId") Long metadataId, @RequestBody String data) throws AuthorizationException, IOException, NonUniqueAccessionException {
		final Metadata metadata = datasetService.getDataset(metadataId);
		if (metadata == null) {
			throw new ResourceNotFoundException();
		}

		// Check for 'WRITE' permission
		datasetService.touch(metadata);

		LOG.debug(data);
		final ObjectMapper objectMapper = new ObjectMapper();
		JsonNode json = null;

		try {
			json = objectMapper.readTree(data);
			LOG.debug(json);
		} catch (final IOException e) {
			LOG.error(e);
			throw e;
		}

		if (json.isArray()) {
			for (final JsonNode jo : json) {
				upsertEntry(metadata, jo);
			}
		} else {
			upsertEntry(metadata, json);
		}

		return null;
	}

	private void upsertEntry(final Metadata metadata, JsonNode json) throws NonUniqueAccessionException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Upserting " + json);
		}

		final DataJson dataJson = readDataJson(json);
		final Accession accession = genesysService.getAccession(dataJson);
		if (accession == null) {
			throw new NullPointerException("No accession for " + dataJson);
		}

		LOG.info("Upserting data for accession " + accession);

		datasetService.upsertAccessionData(metadata, accession.getAccessionId(), dataJson.methodValues);
	}

	private DataJson readDataJson(JsonNode json) {
		final DataJson dataJson = new DataJson();

		for (final Iterator<Entry<String, JsonNode>> fields = json.fields(); fields.hasNext();) {
			final Entry<String, JsonNode> field = fields.next();

			final String key = field.getKey();
			final JsonNode value = field.getValue();

			if (Api1Constants.Accession.INSTCODE.equals(key)) {
				dataJson.instCode = value.asText();
			} else if (Api1Constants.Accession.ACCENUMB.equals(key)) {
				dataJson.acceNumb = value.asText();
			} else if (Api1Constants.Accession.GENUS.equals(key)) {
				dataJson.genus = value.asText();
			} else if (key.startsWith("gm:")) {
				// Handle Genesys Method!
				final long methodId = Long.parseLong(key.substring(3));

				// Need to validate method
				final Method method = traitService.getMethod(methodId);
				if (method == null) {
					throw new NullPointerException("No method with id=" + methodId);
				}

				if (value.isArray()) {
					for (final JsonNode v : value) {
						dataJson.addMethodValue(methodId, convert(method, v));
					}
				} else {
					dataJson.addMethodValue(methodId, convert(method, value));
				}
			}
		}

		return dataJson;
	}

	private Object convert(Method method, JsonNode v) {
		if (v == null) {
			return null;
		}

		if (method.isCoded()) {
			// TODO Handle coded descriptors
			throw new UnsupportedOperationException("Handling coded descriptors is supported");
		} else if (method.getFieldType() == 0) {
			// String
			return v.isNull() ? null : v.asText();
		} else if (method.getFieldType() == 1) {
			// Double
			return v.isNumber() ? v.doubleValue() : null;
		} else if (method.getFieldType() == 2) {
			// Long
			return v.isNumber() ? v.longValue() : null;
		}

		throw new UnsupportedOperationException("Unknown method#fieldType " + method.getFieldType());
	}

	public static class DataJson implements AccessionIdentifier3 {
		public String instCode;
		public String acceNumb;
		public String genus;
		private final Map<Long, List<Object>> methodValues = new HashMap<Long, List<Object>>();

		@Override
		public String getHoldingInstitute() {
			return instCode;
		}

		public void addMethodValue(long methodId, Object value) {
			// if (value == null) {
			// // skip nulls
			// return;
			// }
			List<Object> values = methodValues.get(methodId);
			if (values == null) {
				methodValues.put(methodId, values = new ArrayList<Object>(5));
			}
			values.add(value);
		}

		@Override
		public String getAccessionName() {
			return acceNumb;
		}

		@Override
		public String getGenus() {
			return genus;
		}

		@Override
		public String toString() {
			return MessageFormat.format("AID3 instCode={0} acceNumb={1} genus={2}", instCode, acceNumb, genus);
		}
	}

	public static class MetadataJson {
		@NotBlank
		@NotNull
		public String institute;

		@NotBlank
		@NotNull
		@MinLength(10)
		@MaxLength(200)
		public String title;

		public String SDate;
		public String EDate;
		public String location;
		public Double longitude;
		public Double latitude;
		public Double elevation;
		public String citation;
		public String description;
	}
}
