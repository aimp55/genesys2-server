/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.UUID;

import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.genesys.AccessionHistoric;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.DSService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.TraitService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/archive")
public class ArchiveController extends BaseController {

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private TraitService traitService;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private CropService cropService;

	@Autowired
	private DSService dsService;

	@RequestMapping(value = "/{uuid:.{36}}", method = RequestMethod.GET)
	public String view(ModelMap model, @PathVariable(value = "uuid") UUID uuid) {
		_logger.debug("Viewing archive ACN " + uuid);
		fillModel(model, uuid);
		return "/accession/details";
	}

	@RequestMapping(value = "/{uuid:.{36}}", method = RequestMethod.GET, headers = "accept=text/turtle", produces = "text/turtle")
	public String viewTurtle(ModelMap model, @PathVariable(value = "uuid") UUID uuid) {
		_logger.debug("Viewing archive ACN " + uuid);
		fillModel(model, uuid);
		return "/accession/details-turtle";
	}

	private void fillModel(ModelMap model, UUID uuid) {
		final AccessionHistoric accession = genesysService.getHistoricAccession(uuid);
		if (accession == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("accession", accession);
		model.addAttribute("accessionNames", genesysService.listAccessionNames(accession.getAccessionId()));
		model.addAttribute("accessionAliases", genesysService.listAccessionAliases(accession.getAccessionId()));
		model.addAttribute("accessionExchange", genesysService.listAccessionExchange(accession.getAccessionId()));
		model.addAttribute("accessionCollect", genesysService.listAccessionCollect(accession.getAccessionId()));
		model.addAttribute("accessionBreeding", genesysService.listAccessionBreeding(accession.getAccessionId()));
		AccessionGeo accessionGeo = genesysService.listAccessionGeo(accession.getAccessionId());
		model.addAttribute("accessionGeo", accessionGeo);
		model.addAttribute("svalbardData", genesysService.getSvalbardData(accession.getAccessionId()));
		model.addAttribute("accessionRemarks", genesysService.listAccessionRemarks(accession.getAccessionId()));

		model.addAttribute("metadatas", genesysService.listMetadata(accession.getAccessionId()));
		model.addAttribute("methods", traitService.listMethods(accession.getAccessionId()));
		model.addAttribute("methodValues", genesysService.getAccessionTraitValues(accession.getAccessionId()));

		model.addAttribute("crops", cropService.getCrops(accession.getTaxonomy()));

		// Worldclim data
		if (accessionGeo != null && accessionGeo.getTileIndex() != null) {
			DS worldClimDataset = dsService.loadDatasetByUuid(AccessionController.WORLDCLIM_DATASET_UUID);
			if (worldClimDataset != null) {
				model.addAttribute("worldclimJson", dsService.jsonForTile(worldClimDataset, accessionGeo.getTileIndex()));
			}
		}
	}

}
