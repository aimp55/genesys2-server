/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.elasticsearch.client.TransportClientFactoryBean;

@Configuration
@Profile({ "aws", "cacher" })
public class ElasticsearchConfigAWS {

	private String clusterName = "elasticsearch-genesys";

	@Bean
	public TransportClientFactoryBean tcfb() {
		TransportClientFactoryBean tcfb = new TransportClientFactoryBean();
		tcfb.setClusterName(clusterName);
		tcfb.setClusterNodes("localhost:9300");
		tcfb.setClientPingTimeout("10s");
		return tcfb;
	}
}
