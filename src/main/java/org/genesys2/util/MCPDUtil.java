/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class MCPDUtil {
	static Pattern mcpdSplit = Pattern.compile("\\w*;\\w*");

	public static String toMcpdArray(List<Integer> integers) {
		if (integers == null || integers.size() == 0)
			return null;
		String s = "";
		for (Integer i : integers) {
			if (i == null)
				continue;
			if (s.length() > 0)
				s += ";";
			s += i;
		}
		return StringUtils.defaultIfBlank(s, null);
	}

	public static List<Integer> toIntegers(String ints) {
		if (StringUtils.isBlank(ints))
			return null;
		List<Integer> is = new ArrayList<Integer>();
		for (String i : mcpdSplit.split(ints)) {
			if (StringUtils.isBlank(i))
				continue;
			try {
				is.add(Integer.parseInt(i));
			} catch (NumberFormatException e) {

			}
		}
		return is;
	}

	public static List<String> toStrings(String strings) {
		if (StringUtils.isBlank(strings))
			return null;
		return Arrays.asList(mcpdSplit.split(strings));
	}

}
