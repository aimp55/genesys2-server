<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="userprofile.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="userprofile.page.title" />
	</h1>

	<security:authorize access="hasRole('ADMINISTRATOR') || (isAuthenticated() && principal.user.id == #user.id)">
		<a href="<c:url value="/profile/${user.uuid}/edit" />" class="close"> <spring:message code="edit" /></a>
	</security:authorize>

	<div class="form-horizontal">
		<div class="form-group">
			<label for="password" class="col-lg-2 control-label"><spring:message code="user.full-name" /></label>
			<div class="col-lg-5">${user.name}</div>
		</div>

		<security:authorize access="hasRole('ADMINISTRATOR') || (isAuthenticated() && principal.user.id == #user.id)">
			<div class="form-group">
				<label for="password" class="col-lg-2 control-label"><spring:message code="user.email" /></label>
				<div class="col-lg-5">${user.email}</div>
			</div>

			<div class="form-group">
				<label class="col-lg-2 control-label"><spring:message code="user.account-status" /></label>
				<div class="col-lg-5">
					<c:if test="${user.systemAccount}">SYSTEM</c:if>
					<c:if test="${not user.enabled}"><spring:message code="user.account-disabled" /></c:if>
					<c:if test="${user.accountLocked}"><spring:message code="user.account-locked-until" />
						<fmt:formatDate value="${user.lockedUntil}" type="time" />
					</c:if>
				</div>
			</div>

			<div class="form-group">
				<label class="col-lg-2 control-label"><spring:message code="user.roles" /></label>
				<div class="col-lg-10">
					<c:forEach items="${user.roles}" var="role">
						<div>${role}</div>
					</c:forEach>
				</div>
			</div>

		</security:authorize>


	</div>

	<div class="form-group">

	<security:authorize access="hasRole('ADMINISTRATOR')">
		<button class="btn" id="acccount-lock">Lock</button>
		<button class="btn" id="acccount-unlock">Unlock</button>

		<button class="btn" id="acccount-disable">Disable</button>
		<button class="btn" id="acccount-enable">Enable</button>
	</div>
	</security:authorize>

    <div class="form-group">
                <security:authorize access="hasRole('ADMINISTRATOR') && (isAuthenticated() && principal.user.id == #user.id)">
                    <a href="<c:url value="/management/allTokens" />" class="btn btn-default"> <spring:message code="oauth-client.list" /></a>
                </security:authorize>
                <security:authorize access="hasRole('ADMINISTRATOR') || (isAuthenticated() && principal.user.id == #user.id)">
                    <a href="<c:url value="/management/user/${user.uuid}/tokens" />" class="btn btn-default"><spring:message code="oauth-client.issued.tokens" /></a>
                </security:authorize>
                <security:authorize access="hasRole('ADMINISTRATOR') || principal.user.id == #user.id">
                	<c:if test="${not user.hasRole('VALIDATEDUSER')}">
                    <a href="<c:url value="/profile/${user.uuid}/send"/>"  class="btn btn-default"/>Send validation email</a>
                    </c:if>
                </security:authorize>
                <security:authorize access="hasAnyRole('VETTEDUSER','ADMINISTRATOR')">
                     <a href="<c:url value="/management/addClient" />" class="btn btn-default"><spring:message code="client.details.add" /></a>
                </security:authorize>
                <security:authorize access="hasRole('ADMINISTRATOR')">
                	<c:if test="${not user.hasRole('VETTEDUSER')}">
					<a href="<c:url value="/profile/${user.uuid}/vetted-user"/> "class="btn btn-default"/>Vetted user</a>
					</c:if>
	                <security:authorize access="isAuthenticated()">
	                    <a href="#" data-toggle="modal"  data-target="#myModal" class="btn btn-default get_widget" id="get_widget">Get Widget</a>
	                </security:authorize>
                </security:authorize>
    </div>

	<h3><spring:message code="team.user-teams" /></h3>

	<ul class="funny-list">
	<c:forEach items="${teams}" var="team" varStatus="status">
		<li class="${status.count % 2 == 0 ? 'even' : 'odd'}"><a href="<c:url value="/team/${team.uuid}" />"><c:out value="${team.name}" /></a>

			<security:authorize access="isAuthenticated() && principal.user.id == #user.id">
			<a x-team-id="${team.id}" class="pull-right"><spring:message code="team.leave-team" /></a>
			</security:authorize>
		</li>
	</c:forEach>
	</ul>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
    </div>
    <!--End modal-->
	<security:authorize access="isAuthenticated() && principal.user.id == #user.id">
	<h4><spring:message code="team.create-new-team" /></h4>
	<form id="new-team-form" class="form-horizontal">
		<div class="form-group">
			<label for="team-name" class="col-lg-2 control-label"><spring:message code="team.team-name" /></label>
			<div class="col-lg-3"><input type="text" name="name" id="team-name" class="span3 form-control" /></div>
			<div class="col-lg-1">
				<input type="submit" value="<spring:message code="create" />" class="btn btn-primary" />
			</div>
		</div>
	</form>
	</security:authorize>

<content tag="javascript">
	<security:authorize access="isAuthenticated()">
	<script src="<c:url value="/html/js/main.js" />"></script>
	<script src="<c:url value="/html/js/jsonclient.js" />"></script>
	<script type="text/javascript">
	jQuery(document).ready(function() {



	<security:authorize access="principal.user.id == #user.id">
		$("#new-team-form input[type=submit]").on("click", function(e) {
			e.preventDefault();
			x01("<c:url value="/json/v0/me/teams" />", { success: function(e) {
				window.location.reload();
			}}, $(this.form).serializeObject());
		});

		$("a[x-team-id]").on("click", function(e) {
			e.preventDefault();
			x01("<c:url value="/json/v0/me/teams/" />" + $(this).attr('x-team-id') + "/leave",
					{ success: function(e) {
						window.location.reload();
					}});
		});
	</security:authorize>

	<security:authorize access="hasRole('ADMINISTRATOR')">
		$("button#acccount-enable").on("click", function(e) {
			e.preventDefault();
			x01("<c:url value="/json/v0/user/${user.uuid}/enabled" />", { success: function(e) {
				window.location.reload();
			}}, true);
		});
		$("button#acccount-disable").on("click", function(e) {
			e.preventDefault();
			x01("<c:url value="/json/v0/user/${user.uuid}/enabled" />", { success: function(e) {
				window.location.reload();
			}}, false);
		});
		$("button#acccount-lock").on("click", function(e) {
			e.preventDefault();
			x01("<c:url value="/json/v0/user/${user.uuid}/locked" />", { success: function(e) {
				window.location.reload();
			}}, true);
		});
		$("button#acccount-unlock").on("click", function(e) {
			e.preventDefault();
			x01("<c:url value="/json/v0/user/${user.uuid}/locked" />", { success: function(e) {
				window.location.reload();
			}}, false);
		});
	</security:authorize>
	});
	</script>
	</security:authorize>
</content>
</body>
</html>