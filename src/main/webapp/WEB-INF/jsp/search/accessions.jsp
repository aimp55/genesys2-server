<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<title><spring:message code="search.page.title" /></title>
</head>
<body>
	<h1><spring:message code="search.page.title" /></h1>
	
	<div class="main-col-header clearfix">
		<div class="nav-header">
			<div class="results"><spring:message code="paged.totalElements" arguments="${pagedData == null ? 0 : pagedData.totalElements}" /></div>
			<form method="get" action="search">
			<input type="hidden" name="q" value="<c:out value="${q}" />" />
			<div class="pagination">
				<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
				<a class="${pagedData.number eq 0 ? 'disabled' :''}" href="?q=<c:out value="${q}" />&amp;page=${pagedData.number eq 0 ? 1 : pagedData.number}"><spring:message code="pagination.previous-page" /></a>
				<input class="form-control" style="display: inline; max-width: 5em; text-align: center" type="text" name="page" placeholder="${pagedData.number + 1}" />
				<a href="?q=<c:out value="${q}" />&amp;page=${pagedData.number + 2}"><spring:message code="pagination.next-page" /></a>
			</div>
			</form>
		</div>
	</div>
	

	<c:if test="${error ne null}">
		<div class="alert alert-warning">
			<spring:message code="search.search-query-failed" arguments="${error.message}" />
		</div>
	</c:if>
	<c:if test="${pagedData eq null}">
		<div class="alert alert-warning">
			<spring:message code="search.search-query-missing" />
		</div>
	</c:if>
		
	<div class="applied-filters">
		<form class="" method="get" action="<c:url value="/acn/search" />">
			<div class="row">
				<div class="col-md-4"><input type="text" placeholder="<spring:message code="search.input.placeholder" />" name="q" class="form-control" value="<c:out value="${q}" />" /></div>
				<div class="col-md-2"><input type="submit" value="<spring:message code="search.button.label" />" class="btn" /></div>
			</div>
		</form>
	</div>

	<c:if test="${pagedData ne null and pagedData.totalElements gt 0}">
	<table class="accessions">
			<thead>
				<tr>
					<td class="idx-col"></td>
					<td />
					<td><spring:message code="accession.accessionName" /></td>
					<%--<td><spring:message code="accession.otherNames" /></td>--%>
					<td><spring:message code="accession.taxonomy" /></td>
					<td class="notimportant"><spring:message code="accession.origin" /></td>
					<td class="notimportant"><spring:message code="accession.sampleStatus" /></td>
					<td class="notimportant"><spring:message code="accession.holdingInstitute" /></td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
					<tr class="acn">
						<td class="idx-col">${status.count + pagedData.size * pagedData.number}</td>
						<td class="sel" x-aid="${accession.id}"></td>
						<td><a href="<c:url value="/acn/id/${accession.id}" />"><b>
							<c:out value="${accession.acceNumb}" />
						</b></a></td>
						<%-- <td><c:forEach items="${accession.aliases}" var="alias"><c:out value="${alias.name}" /> </c:forEach> --%>
						</td>
						<%-- <td><a href="<c:url value="/acn/t/${accession.taxonomy.genus}/${accession.taxonomy.species}" />"><c:out value="${accession.taxonomy.sciName}" /></a></td> --%>
						<td><c:out value="${accession.taxonomy.sciName}" /></td>
						<%-- <td class="notimportant"><a href="<c:url value="/geo/${accession.orgCty.iso3}" />"><c:out value="${accession.orgCty.name}" /></a></td> --%>
						<td class="notimportant"><c:out value="${jspHelper.getCountry(accession.orgCty.iso3).getName(pageContext.response.locale)}" /></td>
						<td class="notimportant"><spring:message code="accession.sampleStatus.${accession.sampStat}" /></td>
						<td class="notimportant"><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if>
</body>
</html>