<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="acl.page.permission-manager"/></title>
</head>
<body>
<h1>
    <small><c:out value="${aclObjectIdentity.aclClass.aclClass}"/></small>
    <c:out value="${aclObjectIdentity.objectIdIdentity}"/>
</h1>


<p><spring:message code="acl.owner"/>: <c:out value="${jspHelper.userByUuid(aclObjectIdentity.ownerSid.sid).email}"/></p>
<table class="accessions">
    <thead>
    <tr>
        <td><spring:message code="acl.sid" /></td>
        <c:forEach items="${aclPermissions}" var="aclPermission">
            <td><spring:message code="acl.permission.${aclPermission.mask}" /></td>
        </c:forEach>
    </tr>
    </thead>
    <tbody>

    <c:forEach items="${aclSids}" var="aclSid" varStatus="status">
        <tr class="${status.count % 2 == 0 ? 'even' : 'odd'}">
            <td><c:out value="${jspHelper.userByUuid(aclSid.sid).email}" /></td>

            <input type="hidden" name="aclSid" class="aclSid" value="${aclSid.sid}"/>

            <c:forEach items="${aclPermissions}" var="aclPermission">
                <td><input type="checkbox" value="1" class="check"
                           name="permissionValue${aclPermission.mask}"
                           id="permissionValue${aclPermission.mask}" disabled="disabled"
                    ${aclEntries[aclSid.sid][aclPermission.mask] ? 'checked' : '' }/></td>
            </c:forEach>

            <td><input type="button" class="btn btn-primary edit" value="<spring:message code="edit" />" /></td>
            <td><input type="submit" class="btn btn-primary save" style="display: none"  value="<spring:message code="save"/>">
                <button class="btn btn-default cancel" style="display: none"><spring:message code="cancel"/></button></td>
        </tr>
    </c:forEach>

    <tr id="permissionAdder" class="${aclSids.size()-1 % 2 == 0 ? 'even' : 'odd'}">
        <td><input type="text" class="required form-control" name="uuid" id="autocomplete" /></td>

        <c:forEach items="${aclPermissions}" var="aclPermission">
            <td><input type="checkbox" id="autoCheck${aclPermission.mask}" value="1"
                       name="acPermissionValue${aclPermission.mask}"
                ${aclEntries[aclSid.sid][aclPermission.mask] ? 'checked' : '' }/></td>
        </c:forEach>

        <td><input type="button" class="btn btn-primary" value="<spring:message code="add" />" /></td>
        <td></td>
    </tr>
    </tbody>
</table>

<a href="<c:url value="${backUrl}" />" class="btn btn-default"><spring:message code="cancel" /></a>

<content tag="javascript">
<script type="text/javascript">
    jQuery(document).ready(function() {
        $("#permissionAdder input[type=button]").on("click", function(a,b,c) {

            var create=$("#autoCheck4").is(':checked');
            var read=$("#autoCheck1").is(':checked');
            var write=$("#autoCheck2").is(':checked');
            var remove=$("#autoCheck8").is(':checked');
            var manage=$("#autoCheck16").is(':checked');

            var object = { "oid": ${aclObjectIdentity.objectIdIdentity},"clazz":"${aclObjectIdentity.aclClass.aclClass}","uuid":$("#permissionAdder input[type=text]")[0].value,"principal":true,
                "create":create,"read":read,"write":write,"delete":remove,"manage":manage};
            //debugger;
            $.ajax("<c:url value="/json/v0/permission/add" />", {
                type : 'POST',
                dataType : 'json',
                contentType: 'application/json; charset=utf-8',
                data: (object==null ? null : JSON.stringify(object)),
                beforeSend : function(xhr) {

                },
                success : function(respObject) {
                    window.location.reload();
                    console.log(respObject);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        });

        $(".save").on("click", function() {

            var create=$(this).parent().parent().find('#permissionValue4').is(':checked');
            var read=$(this).parent().parent().find('#permissionValue1').is(':checked');
            var write=$(this).parent().parent().find('#permissionValue2').is(':checked');
            var remove=$(this).parent().parent().find('#permissionValue8').is(':checked');
            var manage=$(this).parent().parent().find('#permissionValue16').is(':checked');
            var uuid=$(this).parent().parent().find('.aclSid').val();

            var object = { "oid": ${aclObjectIdentity.objectIdIdentity},"clazz":"${aclObjectIdentity.aclClass.aclClass}","uuid":uuid,"principal":true,
                "create":create,"read":read,"write":write,"delete":remove,"manage":manage};

            $.ajax("<c:url value="/json/v0/permission/update" />", {
                type : 'POST',
                dataType : 'json',
                contentType: 'application/json; charset=utf-8',
                data: (object==null ? null : JSON.stringify(object)),
                beforeSend : function(xhr) {

                },
                success : function(respObject) {
                    window.location.reload();
                    console.log(respObject);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        });

        $(".edit").click(function(){
            $(".check").prop("disabled",true);
            $("input:submit").hide();
            $(".cancel").hide();
            $(this).parent().parent().find('input:checkbox').prop("disabled",false);
            $(this).parent().parent().find('input:submit').show();
            $(this).parent().parent().find('.cancel').show();
        })

        $(".cancel").click(function(){
            $(".check").prop("disabled",true);
            $("input:submit").hide();
            $(this).hide();
        })


        $(function () {
            var tags = [];
            <c:forEach items="${userNames}" var="userName">
            tags.push("${userName}");
            </c:forEach>
            $("#autocomplete").autocomplete(
	            { delay: 200, minLength: 4, source: "<c:url value="/json/v0/permission/autocompleteuser" />",  
				messages: { noResults: '', results: function() {} } }
            );

        });
    });
</script>
</content>
</body>
</html>
