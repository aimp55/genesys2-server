<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<title><spring:message code="admin.kpi.index.page" /></title>
</head>
<body>
  <h1>
    <spring:message code="admin.kpi.index.page" />
  </h1>

  <%@ include file="/WEB-INF/jsp/admin/menu.jsp"%>

  <div class="crop-details">
    <c:forEach items="${executions}" var="execution">
      <div class="row">
        <div class="col-xs-3">
          <c:out value="${execution.name}" />
        </div>
        <div class="col-xs-5">
          <a href="<c:url value="/admin/kpi/exec/${execution.name}" />"><c:out value="${execution.title}" /></a>
        </div>
        <div class="col-xs-4">
          <c:out value="${execution.parameter.title}" />
          <small><b><c:out value="${execution.parameter.entity}" /></b> <c:out
              value="${execution.parameter.condition}"
            /></small>
        </div>
        <div class="col-xs-12">
          <c:forEach items="${execution.executionDimensions}" var="dimension">
            <div class="col-xs-6">
              <c:out value="${dimension.dimension.title}" />
              <c:out value="${dimension.dimension.condition}" />
            </div>
            <div class="col-xs-6">
              <c:out value="${dimension.field}" />
              <c:out value="${dimension.link}" />
            </div>
          </c:forEach>
        </div>
      </div>
    </c:forEach>
  </div>

</body>
</html>