<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="country.page.profile.title" arguments="${country.getName(pageContext.response.locale)}" argumentSeparator="|" /></title>
</head>
<body>
	<h1>
		<c:out value="${country.getName(pageContext.response.locale)}" />
		<img class="country-flag bigger" src="<c:url value="${cdnFlagsUrl}" />/${country.code3.toUpperCase()}.png" />
	</h1>

	<form role="form" class="" action="<c:url value="/geo/${country.code3}/update" />" method="post">
		<div class="form-group">
			<label for="blurp-body" class="control-label"><spring:message code="blurp.blurp-body" /></label>
			<div class="controls">
				<textarea id="blurp-body" name="blurp" class="span9 required html-editor">
					<c:out value="${blurp.body}" />
				</textarea>
			</div>
		</div>

		<input type="submit" value="<spring:message code="blurp.update-blurp"/>" class="btn btn-primary" />
		<a href="<c:url value="/geo/${country.code3}" />" class="btn btn-default"> <spring:message code="cancel" />
		</a>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

<content tag="javascript">	
  <script type="text/javascript">
	<local:tinyMCE selector="#blurp-body.html-editor" />
  </script>
</content>
</body>
</html>