<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="resolver.page.index.title" /></title>
</head>
<body>
  <h1>
    <spring:message code="resolver.page.index.title" />
  </h1>

  <form class="form-horizontal" method="post" role="form" action="<c:url value="/resolver/reverse" />">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <div class="form-group">
      <label class="control-label col-sm-4"><spring:message code="resolver.acceNumb" /></label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="acceNumb" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4"><spring:message code="resolver.instCode" /></label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="instCode" />
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-4 col-sm-8">
        <button class="btn btn-primary" type="submit">
          <spring:message code="resolver.lookup" />
        </button>
      </div>
    </div>
  </form>


  <form class="form-horizontal" method="post" role="form" action="<c:url value="/resolver/forward" />">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <div class="form-group">
      <label class="control-label col-sm-4"><spring:message code="resolver.uuid" /></label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="uuid" />
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-4 col-sm-8">
        <button class="btn btn-primary" type="submit">
          <spring:message code="resolver.resolve" />
        </button>
      </div>
    </div>
  </form>


</body>
</html>