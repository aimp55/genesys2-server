<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="client.details.token.list"/></title>
</head>
<body>
<h1>
    <spring:message code="client.details.token.list"/>
</h1>

<p></p>
<table class="accessions">
    <thead>
    <tr>
        <td><spring:message code="oauth-client"/></td>
        <td><spring:message code="oauth-client.token.issue.date"/></td>
        <td><spring:message code="oauth-client.expires.date"/></td>
        <td><spring:message code="edit"/></td>
    </tr>
    </thead>

    <tbody>

    <c:forEach items="${tokens}" var="token">
        <tr class="${token.accessToken.expired ? 'expired' : ''}">
            <td>
                <a href="<c:url value="/management/${token.clientId}"/> ">${token.clientId}</a>
            </td>
            <td>
                <fmt:formatDate value="${token.createdDate}" type="both" timeStyle="full" dateStyle="short" />
            </td>
            <td>
                <fmt:formatDate value="${token.accessToken.expiration}" type="both" timeStyle="full" dateStyle="short" />
            </td>
            <td>
            	<!-- FIXME Use POST -->
                <a href="<c:url value="/management/user/${token.userName}/${token.tokenId}/remove"/>"><spring:message
                        code="oauth-client.remove"/></a>
            </td>
        </tr>
    </c:forEach>

    </tbody>
</table>
</body>
</html>
