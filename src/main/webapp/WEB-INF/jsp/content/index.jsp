<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="content.page.list.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="content.page.list.title" />
	</h1>

	<div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<div class="results"><spring:message code="paged.totalElements" arguments="${pagedData.totalElements}" /></div>
		<div class="pagination">
			<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
			<a class="${pagedData.number eq 0 ? 'disabled' :''}" href="?page=${pagedData.number eq 0 ? 1 : pagedData.number}"><spring:message code="pagination.previous-page" /></a> <a href="?page=${pagedData.number + 2}"><spring:message code="pagination.next-page" /></a>
		</div>
        <div class="form-group">
            <div class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><c:out value="Languge filtering" /> <b class="caret"></b></a>
                <ul  class="dropdown-menu" role="menu" id="transifiex_langs">
                    <li><a href="${pageContext.request.contextPath}/content">All</a></li>
                    <c:forEach items="${languages}" var="language">
                        <li><a href="?language=${language.value}">${language.key}</a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
	</div>
	</div>
	
	<table class="">
		<thead>
			<tr>
				<td><spring:message code="article.slug" /></td>
				<td><spring:message code="article.lang" /></td>
				<td><spring:message code="article.classPk" /></td>
				<td><spring:message code="article.title" /></td>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${pagedData.content}" var="article" varStatus="status">
			<tr class="${status.count % 2 == 0 ? 'even' : 'odd'}">
				<td><a href="<c:url value="/content/${article.slug}/edit/${article.lang}" />">${article.slug}</a></td>
				<td>${article.lang}</td>
				<td>${article.classPk.className} ${article.targetId}</td>
				<td>${article.title}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>


</body>
</html>