<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="data-overview" /></title>
</head>
<body>
	<h1>
		<spring:message code="data-overview" />
	</h1>

	<div class="main-col-header clearfix">
		<div class="row">
			<div class="col-xs-9 pull-right text-right">
				<a class="btn btn-default" href="<c:url value="/explore"><c:param name="filter" value="${jsonFilter}" /></c:url>"><span class="glyphicon glyphicon-eye-open"></span><span style="margin-left: 0.5em;"><spring:message code="view.accessions" /></span></a>
				<a class="btn btn-default" href="<c:url value="/explore/map"><c:param name="filter" value="${jsonFilter}" /></c:url>"><span class="glyphicon glyphicon-globe"></span><span style="margin-left: 0.5em;"><spring:message code="maps.view-map" /></span></a>
			</div>
			<div class="col-xs-3 results">
				<a class="btn btn-default" href="javascript: window.history.go(-1);"><spring:message code="navigate.back" /></a>
			</div>
		</div>
	</div>
	
	<c:if test="${fn:length(currentFilters) gt 0}">
	<div id="allfilters" class="disabled applied-filters">
		<%-- Only render currently present filters --%>
        <c:forEach items="${currentFilters}" var="filter">

            <c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}"/>
            <c:set var="appliedFilter" value="${appliedFilters.get(filter.key)}" />

            <div class="clearfix filter-block" id="<c:out value="${normalizedKey}" />_filter" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">
                <div class="col-lg-3 edit-fil">
                    <c:if test="${not filter.core}">
                        <c:out value="${filter.title}" />
                        <%-- <a href="<c:url value="/descriptors/${filter.key}" />"> --%>
                    </c:if>

                    <c:if test="${filter.core}">
						<spring:message code="filter.${filter.key}" />
                    </c:if>
                </div>
                <div class="col-lg-9">
                	<div class="filter-values" id="<c:out value="${normalizedKey}" />_value">
                		<c:if test="${appliedFilter.inverse}">
                			<spring:message code="filter.inverse" />
                		</c:if>
	                    <c:forEach items="${filters[appliedFilter.key]}" var="value">
	                        <c:set var="string" value="${value}"/>
	                        <c:if test="${fn:contains(value, 'range')}">
	                            <c:set var="string" value="${fn:replace(value,'{range=[','Between ')}"/>
	                            <c:set var="string" value="${fn:replace(string,',',' and ')}"/>
	                            <c:set var="string" value="${fn:replace(string,']}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
	                        <c:if test="${fn:contains(value, 'min')}">
	                            <c:set var="string" value="${fn:replace(value,'{min=','More than ')}"/>
	                            <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
	                        <c:if test="${fn:contains(value, 'max')}">
	                            <c:set var="string" value="${fn:replace(value,'{max=','Less than ')}"/>
	                            <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
                            <c:if test="${fn:contains(value, 'like')}">
                                <c:set var="string" value="${fn:replace(value,'{like=','Like ')}"/>
                                <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'}','\"}')}"/>
                            </c:if>

	                        <c:if test="${string==null}">
	                        	<c:set var="string" value="null" />
	                        	<c:set var="value" value="null" />
	                        </c:if>
	                        <div class="filtval complex" x-key="<c:out value="${normalizedKey}" /><c:out value="${value}"/>" i-key="<c:out value="${filter.key}" />"><c:out value="${string}" /></div>
	                        <c:remove var="string" />
	                    </c:forEach>
	                </div>
                </div>
            </div>
        </c:forEach>
    </div>
    </c:if>
	
	<h3><spring:message code="data-overview.institutes" /></h3>	
    <div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
		  <h4 id="stats-instcode"><spring:message code="filter.institute.code" /></h4>
          <local:term-result termResult="${statsInstCode}" type="instCode" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
          <h4 id="stats-instcty"><spring:message code="filter.institute.country.iso3" /></h4>
          <local:term-result termResult="${statsInstCountry}" type="country" />
		</div>
	</div>

	<h3><spring:message code="data-overview.composition" /></h3>	
    <div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
          <h4 id="stats-crops"><spring:message code="filter.crops" /></h4>
          <local:term-result termResult="${statsCrops}" type="crop" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
		  <h4 id="stats-sampstat"><spring:message code="filter.sampStat" /></h4>
          <local:term-result termResult="${statsSampStat}" type="i18n.accession.sampleStatus" />
		</div>
	</div>
	<div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-genus"><spring:message code="filter.taxonomy.genus" /></h4>
            <local:term-result termResult="${statsGenus}" type="genus" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-species"><spring:message code="filter.taxonomy.species" /></h4>
			<local:term-result termResult="${statsSpecies}" type="species" />
		</div>
	</div>
	
	<h3><spring:message code="data-overview.sources" /></h3>	
    <div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
		  <h4 id="stats-orgcty"><spring:message code="filter.orgCty.iso3" /></h4>
          <local:term-result termResult="${statsOrgCty}" type="country" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-donorcode"><spring:message code="data-overview.donorCode" /></h4>
            <local:term-result termResult="${statsDonorCode}" type="instCode" />
		</div>
	</div>
	
	<h3><spring:message code="data-overview.availability" /></h3>	
    <div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
          <h4 id="stats-sampstat"><spring:message code="data-overview.mlsStatus" /></h4>
          <local:term-result termResult="${statsMLS}" type="bool" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-available"><spring:message code="filter.available" /></h4>
            <local:term-result termResult="${statsAvailable}" type="bool" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
		  <h4 id="stats-available"><spring:message code="filter.historic" /></h4>
          <local:term-result termResult="${statsHistoric}" type="bool" />
		</div>
	</div>
	
	<h3><spring:message code="data-overview.management" /></h3>	
	<div class="row">
		<div class="col-xs-12 col-sm-6 row-section">
		  <h4 id="stats-duplsite"><spring:message code="filter.duplSite" /></h4>
          <local:term-result termResult="${statsDuplSite}" count="${accessionCount}" type="instCode" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
		  <h4 id="stats-duplsite"><spring:message code="filter.sgsv" /></h4>
          <local:term-result termResult="${statsSGSV}" type="bool" />
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 row-section">
		  <h4 id="stats-storage"><spring:message code="filter.storage" /></h4>
          <local:term-result termResult="${statsStorage}" count="${accessionCount}" type="i18n.accession.storage" />
		</div>
	</div>

<content tag="javascript">
	<script type="text/javascript">
	
	</script>
</content>
</body>
</html>