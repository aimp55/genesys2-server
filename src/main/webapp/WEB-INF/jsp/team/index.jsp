<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="team.page.list.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="team.page.list.title" />
	</h1>

	<div class="nav-header">
		<spring:message code="paged.totalElements" arguments="${pagedData.totalElements}" />
		<br />
		<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
		<a class="${pagedData.number eq 0 ? 'disabled' :''}" href="?page=${pagedData.number eq 0 ? 1 : pagedData.number}"><spring:message code="pagination.previous-page" /></a> <a href="?page=${pagedData.number + 2}"><spring:message code="pagination.next-page" /></a>
	</div>
	<ul class="funny-list">
		<c:forEach items="${pagedData.content}" var="team" varStatus="status">
			<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
				<a href="<c:url value="/team/${team.uuid}" />"><c:out value="${team.name}" /></a>
			</li>
		</c:forEach>
	</ul>


</body>
</html>