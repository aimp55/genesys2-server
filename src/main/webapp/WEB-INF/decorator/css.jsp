<%@include file="/WEB-INF/jsp/init.jsp"%>

<c:choose>
  <c:when test="${requestContext.theme.name eq 'one'}">
    <link href="<c:url value="/html/styles/all.min.css" />" type="text/css" rel="stylesheet" />
  </c:when>
  <c:when test="${requestContext.theme.name eq 'all'}">
    <link href="<c:url value="/html/styles/bootstrap.min.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/styles/other.min.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/styles/genesys.css" />" type="text/css" rel="stylesheet" />
  </c:when>
  <c:otherwise>
    <link href="<c:url value="/html/styles/bootstrap.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/styles/jquery-ui.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/styles/forza.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/styles/leaflet.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/styles/genesys.css" />" type="text/css" rel="stylesheet" />
  </c:otherwise>
</c:choose>

